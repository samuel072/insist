package com.pan.insist.controller.rest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>获取k8s中的环境变量</p>
 *
 * @author aneng huang
 * @since 2020-09-18 09:54:46
 */

@RestController
@RequestMapping("/rest")
public class K8sConfigController {

    @Value("${spring.datasource.url:null}")
    private String url;

    @Value("${spring.datasource.password:null}")
    private String password;

    @GetMapping("/get-config")
    public Map<String, Object> getConfig() {
        Map<String, Object> map = new HashMap<>();
        map.put("mysql-port", url);
        map.put("password", password);
        return map;
    }
}
