package com.pan.insist.controller.web;

import com.alibaba.fastjson.JSONObject;
import com.pan.insist.annotation.CacheLock;
import com.pan.insist.annotation.OperateLog;
import com.pan.insist.base.BaseController;
import com.pan.insist.constant.CommonConstant;
import com.pan.insist.constant.OperateConstant;
import com.pan.insist.util.FastDFSUtil;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;

/**
 * <p>
 *  文件上传<br/>
 *  测试fastdfs文件系统
 * </p>
 *
 * @author kaiji
 * @since 2019/12/9 22:19
 */
@Log4j2
@Controller
@RequestMapping("/upload")
public class UploadController extends BaseController {

    @Resource
    private FastDFSUtil fastDFSUtil;

    @GetMapping("upload")
    public String toUpload() {
        return "upload";
    }

    @ResponseBody
    @PostMapping("/upload")
    @OperateLog(operateModule = "文件上传", operateType = OperateConstant.SAVE, operateDesc = "上传完整文件")
    public JSONObject upload(@RequestParam("file") MultipartFile file) {
        JSONObject jsonObject = new JSONObject();
        try {
            String uploadPath = fastDFSUtil.upload(file);
            jsonObject.put(CommonConstant.AjaxCode.CODE, CommonConstant.AjaxCode.SUCCESS_CODE);
            jsonObject.put(CommonConstant.AjaxCode.MESSAGE, uploadPath);

        } catch (IOException e) {
            e.printStackTrace();

            jsonObject.put(CommonConstant.AjaxCode.CODE, CommonConstant.AjaxCode.SUCCESS_CODE);
            jsonObject.put(CommonConstant.AjaxCode.MESSAGE, e.getMessage());
        }
        return jsonObject;
    }

    /**
     * 上传图片 并且生成缩略图
     *
     * @param file
     *      上传的文件
     * @return  文件地址
     */
    @ResponseBody
    @PostMapping("/uploadThumb")
    @OperateLog(operateModule = "文件上传", operateType = OperateConstant.SAVE, operateDesc = "上传有缩略的完整文件")
    public JSONObject uploadThumb(@RequestParam("file") MultipartFile file) {
        JSONObject jsonObject = new JSONObject();
        try {
            String uploadPath = fastDFSUtil.uploadCrtThumbImage(file);
            jsonObject.put(CommonConstant.AjaxCode.CODE, CommonConstant.AjaxCode.SUCCESS_CODE);
            jsonObject.put(CommonConstant.AjaxCode.MESSAGE, uploadPath);

        } catch (IOException e) {
            log.error(e.getMessage());

            jsonObject.put(CommonConstant.AjaxCode.CODE, CommonConstant.AjaxCode.SUCCESS_CODE);
            jsonObject.put(CommonConstant.AjaxCode.MESSAGE, e.getMessage());
        }
        return jsonObject;
    }

    @ResponseBody
    @DeleteMapping("/delete")
    @CacheLock(prefix = "upload-delete")
    @OperateLog(operateModule = "文件上传", operateType = OperateConstant.DELETE, operateDesc = "删除服务器上的文件")
    public JSONObject delete() {
        JSONObject jsonObject = new JSONObject();
        try {
            String path = "http://www.huangan.xyz/group1/M00/00/00/rBEAC132MQ6AZQvUAABeqdUrCbw576.jpg";
            fastDFSUtil.delete(path);
            jsonObject.put(CommonConstant.AjaxCode.CODE, CommonConstant.AjaxCode.SUCCESS_CODE);
            jsonObject.put(CommonConstant.AjaxCode.MESSAGE, "删除成功！");

        } catch (Exception e) {
            log.error(e.getMessage());
            jsonObject.put(CommonConstant.AjaxCode.CODE, CommonConstant.AjaxCode.FAIL_CODE);
            jsonObject.put(CommonConstant.AjaxCode.MESSAGE, e.getMessage());
        }
        return jsonObject;
    }

    @ResponseBody
    @GetMapping("/download")
    @CacheLock(prefix = "upload-download")
    @OperateLog(operateModule = "文件上传", operateType = OperateConstant.DELETE, operateDesc = "文件下载")
    public JSONObject download(HttpServletResponse response) {
        JSONObject jsonObject = new JSONObject();
        try {
            String path = "http://www.huangan.xyz/group1/M00/00/00/rBEAC132MQ6AZQvUAABeqdUrCbw576.jpg";
            String fileName = "黄阿能";

            byte[] bytes = fastDFSUtil.download(path);
            response.reset();
            response.setContentType("applicatoin/octet-stream");
            response.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(fileName, "UTF-8"));
            ServletOutputStream out = response.getOutputStream();
            out.write(bytes);
            out.close();

            jsonObject.put(CommonConstant.AjaxCode.CODE, CommonConstant.AjaxCode.SUCCESS_CODE);
            jsonObject.put(CommonConstant.AjaxCode.MESSAGE, "下载成功！");

        } catch (Exception e) {
            log.error(e.getMessage());
            jsonObject.put(CommonConstant.AjaxCode.CODE, CommonConstant.AjaxCode.FAIL_CODE);
            jsonObject.put(CommonConstant.AjaxCode.MESSAGE, e.getMessage());
        }
        return jsonObject;
    }

}
