package com.pan.insist.controller.admin;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pan.insist.annotation.CacheLock;
import com.pan.insist.annotation.OperateLog;
import com.pan.insist.base.BaseController;
import com.pan.insist.constant.CommonConstant;
import com.pan.insist.constant.OperateConstant;
import com.pan.insist.exception.ViewException;
import com.pan.insist.model.ExceptionLogModel;
import com.pan.insist.service.ExceptionLogService;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.Arrays;


/**
* <p>
* 异常日志-前端控制器
* </p>
*
* @author 黄阿能
* @since 2020-01-20
*/

@Log4j2
@Controller
@RequestMapping("/exceptionLog")
public class ExceptionLogController extends BaseController {
    @Resource
    private ExceptionLogService exceptionLogService;

    private final static String PREFIX = "exceptionLog/";

    /**
    * 跳转到异常日志首页
    */
    @GetMapping("/list")
    public String list() {
        return PREFIX + LIST;
    }

    @ResponseBody
    @GetMapping("/listJson")
    // @OperateLog(operateModule = "异常日志", operateType = OperateConstant.SELECT, operateDesc = "查看异常日志列表")
    public JSONObject listJson(Page<ExceptionLogModel> iPage, ExceptionLogModel exceptionLogModel) {
        QueryWrapper<ExceptionLogModel> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotBlank(exceptionLogModel.getExceptionName())) {
            queryWrapper.lambda().like(ExceptionLogModel::getExceptionName, exceptionLogModel.getExceptionName());
        }
        if (StringUtils.isNotBlank(exceptionLogModel.getMethod())) {
            queryWrapper.lambda().like(ExceptionLogModel::getMethod, exceptionLogModel.getMethod());
        }
        if (StringUtils.isNotBlank(exceptionLogModel.getIp())) {
            queryWrapper.lambda().like(ExceptionLogModel::getIp, exceptionLogModel.getIp());
        }
        queryWrapper.lambda().orderByDesc(ExceptionLogModel::getCreateDate);
        iPage = exceptionLogService.page(iPage, queryWrapper);
        return page2LayTablePage(iPage);
    }

    @GetMapping("/view")
    @CacheLock(prefix = "view")
    @OperateLog(operateModule = "异常日志", operateType = OperateConstant.VIEW, operateDesc = "查看异常日志详情")
    public String view(String id, ModelMap modelMap) {
        if (StringUtils.isBlank(id)) {
            throw new ViewException("id不存在");
        }
        ExceptionLogModel exceptionLogModel = exceptionLogService.getById(id);
        modelMap.put("exceptionLogModel", exceptionLogModel);
        return PREFIX + VIEW;
    }

    /**
    * 批量删除
    *
    * @param ids id集合
    */
    @ResponseBody
    @PostMapping("delete")
    @CacheLock(prefix = "exceptionLog-delete")
    @OperateLog(operateModule = "异常日志", operateType = OperateConstant.DELETE, operateDesc = "删除异常日志，可以批量删除")
    public JSONObject delete(String ids) {
        JSONObject jsonObject = new JSONObject();
        if (StringUtils.isEmpty(ids)) {
            jsonObject.put(CommonConstant.AjaxCode.CODE, CommonConstant.AjaxCode.FAIL_CODE);
            jsonObject.put(CommonConstant.AjaxCode.MESSAGE, "请至少选择一条数据！");
            return jsonObject;
        }
        try {
            exceptionLogService.removeByIds(Arrays.asList(ids.split(CommonConstant.DOC_FLAG)));
            jsonObject.put(CommonConstant.AjaxCode.CODE, CommonConstant.AjaxCode.SUCCESS_CODE);
            jsonObject.put(CommonConstant.AjaxCode.MESSAGE, "删除异常日志数据成功！");
        } catch (Exception e) {
            log.error("删除失败！原因： {}", e.getMessage());
            jsonObject.put(CommonConstant.AjaxCode.CODE, CommonConstant.AjaxCode.FAIL_CODE);
            jsonObject.put(CommonConstant.AjaxCode.MESSAGE, "删除异常日志数据失败！");
        }

        return jsonObject;
    }
}
