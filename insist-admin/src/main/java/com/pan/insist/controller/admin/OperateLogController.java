package com.pan.insist.controller.admin;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pan.insist.annotation.CacheLock;
import com.pan.insist.annotation.OperateLog;
import com.pan.insist.base.BaseController;
import com.pan.insist.constant.CommonConstant;
import com.pan.insist.constant.OperateConstant;
import com.pan.insist.exception.ViewException;
import com.pan.insist.model.OperateLogModel;
import com.pan.insist.service.OperateLogService;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.Arrays;


/**
* <p>
* 操作日志-前端控制器
* </p>
*
* @author 黄阿能
* @since 2020-01-20
*/

@Log4j2
@Controller
@RequestMapping("/operateLog")
public class OperateLogController extends BaseController {
    @Resource
    private OperateLogService operateLogService;

    private final static String PREFIX = "operateLog/";

    /**
    * 跳转到操作日志首页
    */
    @GetMapping("/list")
    public String list() {
        return PREFIX + LIST;
    }

    @ResponseBody
    @GetMapping("/listJson")
    // @OperateLog(operateModule = "操作日志管理", operateType = OperateConstant.SELECT, operateDesc = "查看操作日志列表")
    public JSONObject listJson(Page<OperateLogModel> iPage, OperateLogModel operateLogModel) {
        QueryWrapper<OperateLogModel> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotBlank(operateLogModel.getType())) {
            queryWrapper.lambda().eq(OperateLogModel::getType, operateLogModel.getType());
        }
        if (StringUtils.isNotBlank(operateLogModel.getMethod())) {
            queryWrapper.lambda().like(OperateLogModel::getMethod, operateLogModel.getMethod());
        }
        if (StringUtils.isNotBlank(operateLogModel.getModule())) {
            queryWrapper.lambda().like(OperateLogModel::getModule, operateLogModel.getModule());
        }
        if (StringUtils.isNotBlank(operateLogModel.getUserName())) {
            queryWrapper.lambda().like(OperateLogModel::getUserName, operateLogModel.getUserName());
        }
        if (StringUtils.isNotBlank(operateLogModel.getIp())) {
            queryWrapper.lambda().like(OperateLogModel::getIp, operateLogModel.getIp().trim());
        }
        queryWrapper.lambda().orderByDesc(OperateLogModel::getCreateDate);
        queryWrapper.select(OperateLogModel.class, info-> !StringUtils.equals(info.getColumn(), "response_params"));
        iPage = operateLogService.page(iPage, queryWrapper);
        return page2LayTablePage(iPage);
    }

    @GetMapping("/view")
    @CacheLock(prefix = "view")
    @OperateLog(operateModule = "操作日志管理", operateType = OperateConstant.VIEW, operateDesc = "查看操作日志详情")
    public String view(String id, ModelMap modelMap) {
        if (StringUtils.isBlank(id)) {
            throw new ViewException("id不存在");
        }
        OperateLogModel operateLogModel = operateLogService.getById(id);
        modelMap.put("operateLogModel", operateLogModel);
        return PREFIX + VIEW;
    }

    /**
    * 批量删除
    *
    * @param ids id集合
    */
    @ResponseBody
    @PostMapping("delete")
    @CacheLock(prefix = "operateLog-delete")
    @OperateLog(operateModule = "操作日志管理", operateType = OperateConstant.DELETE, operateDesc = "删除操作日志，可以批量删除")
    public JSONObject delete(String ids) {
        JSONObject jsonObject = new JSONObject();
        if (StringUtils.isEmpty(ids)) {
            jsonObject.put(CommonConstant.AjaxCode.CODE, CommonConstant.AjaxCode.FAIL_CODE);
            jsonObject.put(CommonConstant.AjaxCode.MESSAGE, "请至少选择一条数据！");
            return jsonObject;
        }
        try {
            operateLogService.removeByIds(Arrays.asList(ids.split(CommonConstant.DOC_FLAG)));
            jsonObject.put(CommonConstant.AjaxCode.CODE, CommonConstant.AjaxCode.SUCCESS_CODE);
            jsonObject.put(CommonConstant.AjaxCode.MESSAGE, "删除操作日志数据成功！");
        } catch (Exception e) {
            log.error("删除失败！原因： {}", e.getMessage());
            jsonObject.put(CommonConstant.AjaxCode.CODE, CommonConstant.AjaxCode.FAIL_CODE);
            jsonObject.put(CommonConstant.AjaxCode.MESSAGE, "删除操作日志数据失败！");
        }

        return jsonObject;
    }

}
