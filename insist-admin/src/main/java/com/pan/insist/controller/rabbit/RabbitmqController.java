package com.pan.insist.controller.rabbit;

import com.pan.insist.constant.CommonConstant;
import com.pan.insist.util.UuidUtil;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 *
 * </p>
 *
 * @author kaiji
 * @since 2020/1/30 18:13
 */
@RestController
public class RabbitmqController {

    @Resource
    private RabbitTemplate rabbitTemplate;
    @Resource
    private AmqpAdmin amqpAdmin;

    /**
     * 测试点对点的发送消息
     */
    @GetMapping("/sendMsg")
    public void point2Point() {
        // 发送一个点对点的消息
        rabbitTemplate.convertAndSend("admin.huang.direct", "direct.huang",
                "点对点---->😆hello 黄阿能。哈哈");
        System.out.println("发送一个点对点的消息成功");

        Map<String, Object> dataMap = new HashMap<>(CommonConstant.DEFAULT_INITIAL_CAPACITY);
        dataMap.put("uid", UuidUtil.getUuid());
        dataMap.put("username", "黄阿能");
        dataMap.put("age", 28);
        dataMap.put("wife", "朱盼");
        rabbitTemplate.convertAndSend("admin.huang.fanout", "fanout.huang", dataMap);
        System.out.println("发送一条匹配式的消息成功");

        dataMap = new HashMap<>(CommonConstant.DEFAULT_INITIAL_CAPACITY);
        dataMap.put("uid", UuidUtil.getUuid());
        dataMap.put("username", "dog");
        dataMap.put("age", 2);
        dataMap.put("wife", "cat");
        rabbitTemplate.convertAndSend("admin.huang.topic", "", dataMap);
        System.out.println("发送一条发布、订阅式的消息成功");
    }

    @GetMapping("/receiveMsg")
    public Object receive2Point() {
        Object o = rabbitTemplate.receiveAndConvert("huang.queues");
        assert o != null;
        System.out.println(o.getClass());
        return o;
    }

    /**
     * 创建交换机
     */
    @GetMapping("createExchange")
    public void createExchange() {
        // 创建一个点对点、持久化的、自动删除为false的交换机
        Exchange directExchange = new DirectExchange("admin.huang.direct", true, false);
        amqpAdmin.declareExchange(directExchange);
        System.out.println("创建点对点交换机完成");
        Exchange fanoutExchange = new FanoutExchange("admin.huang.fanout", true, false);
        amqpAdmin.declareExchange(fanoutExchange);
        System.out.println("创建匹配式交换机完成");
        Exchange topicExchange = new TopicExchange("admin.huang.topic", true, false);
        amqpAdmin.declareExchange(topicExchange);
        System.out.println("创建发布、订阅交换机完成");
    }

    /**
     * 创建队列
     */
    @GetMapping("createQueues")
    public void createQueues() {
        // 创建队列
        String directQueueName = amqpAdmin.declareQueue(new Queue("admin.huang.direct.queues"));
        System.out.println("创建点对点的队列 ===>" + directQueueName);

        String fanoutQueueName = amqpAdmin.declareQueue(new Queue("admin.huang.fanout.queues"));
        System.out.println("创建匹配式的队列 ===>" + fanoutQueueName);

        String topicQueueName = amqpAdmin.declareQueue(new Queue("admin.huang.topic.queues"));
        System.out.println("创建发布、订阅的队列 ===>" + topicQueueName);
    }

    /**
     * 创建消息交换机与队列的关系
     */
    @GetMapping("createBind")
    public void createBind() {
        // 创建绑定关系
        /**
         * 1. 绑定到那个对象 队列或者消息交换机的名称
         * 2. 绑定的类型，队列或者消息交换机
         * 3. 消息交换机的名称
         * 4. 路由名称
         * 5. 绑定的参数
         */
        Binding directBinding = new Binding("admin.huang.direct.queues", Binding.DestinationType.QUEUE,
                "admin.huang.direct", "direct.huang", null);
        amqpAdmin.declareBinding(directBinding);
        System.out.println("绑定消息交换机与点对点的关系成功!");

        Binding fanoutBinding = new Binding("admin.huang.fanout.queues", Binding.DestinationType.QUEUE,
                "admin.huang.fanout", "fanout.#", null);
        amqpAdmin.declareBinding(fanoutBinding);
        System.out.println("绑定消息交换机与匹配式的关系成功!");


        // 因为订阅与发布所有的队列都可以， 所以路由为空
        Binding topicBinding = new Binding("admin.huang.topic.queues", Binding.DestinationType.QUEUE,
                "admin.huang.topic", "", null);
        amqpAdmin.declareBinding(topicBinding);
        System.out.println("绑定消息交换机与发布、订阅的关系成功!");
    }

    // @RabbitListener(queues={"admin.huang.direct.queues", "admin.huang.fanout.queues", "admin.huang.topic.queues"})
    public void receiveMsgOnAnnotation(Object o) {
        System.out.println("接收到的消息是" + o);
    }
    // @RabbitListeners()
    // public void receiveMsgOnAnnotation() {
    //
    // }


}
