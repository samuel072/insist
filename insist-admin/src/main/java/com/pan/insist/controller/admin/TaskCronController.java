package com.pan.insist.controller.admin;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pan.insist.annotation.CacheLock;
import com.pan.insist.annotation.OperateLog;
import com.pan.insist.base.BaseController;
import com.pan.insist.constant.CommonConstant;
import com.pan.insist.constant.OperateConstant;
import com.pan.insist.exception.ViewException;
import com.pan.insist.model.TaskCronModel;
import com.pan.insist.service.TaskCronService;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.Arrays;


/**
* <p>
* 任务管理-前端控制器
* </p>
*
* @author 黄阿能
* @since 2020-02-07
*/

@Log4j2
@Controller
@RequestMapping("/taskCron")
public class TaskCronController extends BaseController {
    @Resource
    private TaskCronService taskCronService;

    private final static String PREFIX = "taskCron/";

    /**
    * 跳转到任务管理首页
    */
    @GetMapping("/list")
    public String list() {
        return PREFIX + LIST;
    }

    @ResponseBody
    @GetMapping("/listJson")
    @OperateLog(operateModule = "任务管理", operateType = OperateConstant.SELECT, operateDesc = "查询任务管理列表")
    public JSONObject listJson(Page<TaskCronModel> iPage, TaskCronModel taskCronModel) {
        QueryWrapper<TaskCronModel> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotBlank(taskCronModel.getName())) {
            queryWrapper.lambda().like(TaskCronModel::getName, taskCronModel.getName());
        }
        if (StringUtils.isNotBlank(taskCronModel.getCode())) {
            queryWrapper.lambda().like(TaskCronModel::getCode, taskCronModel.getCode());
        }
        iPage = taskCronService.page(iPage, queryWrapper);
        return page2LayTablePage(iPage);
    }

    @GetMapping("/view")
    @OperateLog(operateModule = "任务管理", operateType = OperateConstant.VIEW, operateDesc = "查看任务管理详情")
    public String view(String id, ModelMap modelMap) {
        if (StringUtils.isBlank(id)) {
            throw new ViewException("id不存在");
        }
        TaskCronModel taskCronModel = taskCronService.getById(id);
        modelMap.put("taskCronModel", taskCronModel);
        return PREFIX + VIEW;
    }

    @GetMapping("update")
    @OperateLog(operateModule = "任务管理", operateType = OperateConstant.VIEW, operateDesc = "进入修改任务管理信息页面")
    public String toUpdate(String id, ModelMap modelMap) {
        if (StringUtils.isBlank(id)) {
            throw new ViewException("请选择一条数据！");
        }

        TaskCronModel taskCronModel = taskCronService.getById(id);
        if (taskCronModel == null) {
            log.info("查询不到数据！");
            throw new RuntimeException("请选择一条数据！");
        }
        modelMap.put("taskCronModel", taskCronModel);
        return PREFIX + INPUT;
    }

    @ResponseBody
    @PostMapping("/update")
    @CacheLock(prefix = "taskCron-update")
    @OperateLog(operateModule = "任务管理", operateType = OperateConstant.UPDATE, operateDesc = "修改任务管理信息")
    public JSONObject update(TaskCronModel taskCronModel) {
        JSONObject jsonObject = validate(taskCronModel, CommonConstant.DATA_UPDATE_TYPE);
        Integer code = jsonObject.getInteger(CommonConstant.AjaxCode.CODE);
        if (code.equals(CommonConstant.AjaxCode.FAIL_CODE)) {
            return jsonObject;
        }

        jsonObject = new JSONObject();
        // taskCronModel.setUpdateDate(LocalDateTime.now());
        boolean flag = taskCronService.updateById(taskCronModel);
        if (flag) {
            jsonObject.put(CommonConstant.AjaxCode.MESSAGE, "修改任务管理数据成功！");
            jsonObject.put(CommonConstant.AjaxCode.CODE, CommonConstant.AjaxCode.SUCCESS_CODE);
        } else {
            jsonObject.put(CommonConstant.AjaxCode.MESSAGE, "修改任务管理数据失败！");
            jsonObject.put(CommonConstant.AjaxCode.CODE, CommonConstant.AjaxCode.FAIL_CODE);
        }
        return jsonObject;
    }

    @GetMapping("save")
    @OperateLog(operateModule = "任务管理", operateType = OperateConstant.VIEW, operateDesc = "跳转保存任务管理信息页面")
    public String toSave() {
        return PREFIX + INPUT;
    }

    @ResponseBody
    @PostMapping("save")
    @CacheLock(prefix = "taskCron-save")
    @OperateLog(operateModule = "任务管理", operateType = OperateConstant.SAVE, operateDesc = "保存任务管理信息")
    public JSONObject save(TaskCronModel taskCronModel) {
        JSONObject jsonObject = validate(taskCronModel, CommonConstant.DATA_SAVE_TYPE);
        Integer code = jsonObject.getInteger(CommonConstant.AjaxCode.CODE);
        if (code.equals(CommonConstant.AjaxCode.FAIL_CODE)) {
            return jsonObject;
        }
        jsonObject = new JSONObject();
        try {
            taskCronModel.setCreateDate(LocalDateTime.now());
            taskCronService.save(taskCronModel);
            jsonObject.put(CommonConstant.AjaxCode.CODE, CommonConstant.AjaxCode.SUCCESS_CODE);
            jsonObject.put(CommonConstant.AjaxCode.MESSAGE, "保存数据成功！");
        } catch (Exception e) {
            log.error("保存任务管理数据异常！原因：{}", e.getMessage());
            jsonObject.put(CommonConstant.AjaxCode.CODE, CommonConstant.AjaxCode.FAIL_CODE);
            jsonObject.put(CommonConstant.AjaxCode.MESSAGE, e.getMessage());
        }
        return jsonObject;
    }

    /**
    * 批量删除
    *
    * @param ids id集合
    */
    @ResponseBody
    @PostMapping("delete")
    @CacheLock(prefix = "taskCron-delete")
    @OperateLog(operateModule = "任务管理", operateType = OperateConstant.DELETE, operateDesc = "删除任务管理信息，可批量删除！")
    public JSONObject delete(String ids) {
        JSONObject jsonObject = new JSONObject();
        if (StringUtils.isEmpty(ids)) {
            jsonObject.put(CommonConstant.AjaxCode.CODE, CommonConstant.AjaxCode.FAIL_CODE);
            jsonObject.put(CommonConstant.AjaxCode.MESSAGE, "请至少选择一条数据！");
            return jsonObject;
        }
        try {
            taskCronService.removeByIds(Arrays.asList(ids.split(CommonConstant.DOC_FLAG)));
            jsonObject.put(CommonConstant.AjaxCode.CODE, CommonConstant.AjaxCode.SUCCESS_CODE);
            jsonObject.put(CommonConstant.AjaxCode.MESSAGE, "删除任务管理数据成功！");
        } catch (Exception e) {
            log.error("删除失败！原因： {}", e.getMessage());
            jsonObject.put(CommonConstant.AjaxCode.CODE, CommonConstant.AjaxCode.FAIL_CODE);
            jsonObject.put(CommonConstant.AjaxCode.MESSAGE, "删除任务管理数据失败！");
        }

        return jsonObject;
    }

    protected JSONObject validate(TaskCronModel taskCronModel, int type) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(CommonConstant.AjaxCode.CODE, CommonConstant.AjaxCode.FAIL_CODE);
        if (taskCronModel == null) {
            jsonObject.put(CommonConstant.AjaxCode.MESSAGE, "对象不能为空！");
            return jsonObject;
        }

        if (type == CommonConstant.DATA_UPDATE_TYPE) {
            if (StringUtils.isEmpty(taskCronModel.getId())) {
                jsonObject.put(CommonConstant.AjaxCode.MESSAGE, "请至少选择一个对象数据！");
                return jsonObject;
            }
            // 判断任务管理是否存在
            TaskCronModel taskCron = taskCronService.getById(taskCronModel.getId());
            if (taskCron == null) {
                jsonObject.put(CommonConstant.AjaxCode.MESSAGE, "该数据被删除，已经不存在！");
                return jsonObject;
            }

        } else if (type == CommonConstant.DATA_SAVE_TYPE) {

        } else {
            jsonObject.put(CommonConstant.AjaxCode.CODE, CommonConstant.AjaxCode.FAIL_CODE);
            jsonObject.put(CommonConstant.AjaxCode.MESSAGE, "数据类型错误！");
            return jsonObject;
        }
        jsonObject.put(CommonConstant.AjaxCode.CODE, CommonConstant.AjaxCode.SUCCESS_CODE);
        jsonObject.put(CommonConstant.AjaxCode.MESSAGE, "数据验证成功！");
        return jsonObject;
    }

}
