package com.pan.insist.controller.admin;

import com.alibaba.fastjson.JSONObject;
import com.pan.insist.annotation.OperateLog;
import com.pan.insist.base.BaseController;
import com.pan.insist.constant.CommonConstant;
import com.pan.insist.bean.Context;
import com.pan.insist.bean.GcBean;
import com.pan.insist.constant.OperateConstant;
import com.pan.insist.generator.GeneratorCode;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author kaiji
 */
@Controller
@RequestMapping("code")
public class GeneratorController extends BaseController {

	@Value("${spring.datasource.url}")
	public String url;
	@Value("${spring.datasource.driver-class-name}")
	public String driverName;
	@Value("${spring.datasource.username}")
	public String username;
	@Value("${spring.datasource.password}")
	public String password;

	/** 进入生成代码页面 */
	@GetMapping("")
	public String code() {
		return "/code/code";
	}

	@ResponseBody
	@PostMapping("execute")
	@OperateLog(operateModule = "生成代码", operateType = OperateConstant.SAVE, operateDesc = "生成代码与页面")
	public JSONObject execute(GcBean gb, String bizChName) {
		JSONObject jsonObject = new JSONObject();
		try {
			GeneratorCode gc = new GeneratorCode();
			gb.setUrl(url);
			gb.setDriverName(driverName);
			gb.setUsername(username);
			gb.setPassword(password);
			String[] filedIds = new String[] {"id", "update_date", "create_date"};
			gb.setCommonFiled(filedIds);
			gb.setMapperSwitch(false);
			Context context = new Context();
			context.setBizChName(bizChName);
			String enName = gb.getTableName().replaceAll(gb.getTablePrefix(), "");
			context.setBizEnBigName(enName.substring(0, 1).toUpperCase()+enName.substring(1));
			context.setBizEnName(enName);
			gb.setContext(context);
			gc.generatorCode(gb);
			jsonObject.put(CommonConstant.AjaxCode.CODE, CommonConstant.AjaxCode.SUCCESS_CODE);
			jsonObject.put(CommonConstant.AjaxCode.MESSAGE, "生成代码成功！");
		} catch (Exception e) {
			jsonObject.put(CommonConstant.AjaxCode.CODE, CommonConstant.AjaxCode.FAIL_CODE);
			jsonObject.put(CommonConstant.AjaxCode.MESSAGE, "生成代码失败！原因：" + e);
		}
		return jsonObject;
	}
}
