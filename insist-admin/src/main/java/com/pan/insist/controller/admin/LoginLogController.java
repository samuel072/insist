package com.pan.insist.controller.admin;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pan.insist.annotation.CacheLock;
import com.pan.insist.annotation.OperateLog;
import com.pan.insist.base.BaseController;
import com.pan.insist.constant.CommonConstant;
import com.pan.insist.constant.OperateConstant;
import com.pan.insist.exception.ViewException;
import com.pan.insist.model.LoginLogModel;
import com.pan.insist.service.LoginLogService;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Arrays;


/**
* <p>
* 登录日志-前端控制器
* </p>
*
* @author 黄阿能
* @since 2020-01-16
*/

@Log4j2
@Controller
@RequestMapping("/loginLog")
public class LoginLogController extends BaseController {
    @Resource
    private LoginLogService loginLogService;

    private final static String PREFIX = "log/";

    /**
    * 跳转到登录日志首页
    */
    @GetMapping("/list")
    public String list() {
        return PREFIX + LIST;
    }

    @ResponseBody
    @GetMapping("/listJson")
    @OperateLog(operateModule = "登录日志", operateType = OperateConstant.SELECT, operateDesc = "查看登录日志列表")
    public JSONObject listJson(Page<LoginLogModel> iPage, LoginLogModel loginLogModel) {
        QueryWrapper<LoginLogModel> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotBlank(loginLogModel.getLoginName())) {
            queryWrapper.lambda().like(LoginLogModel::getLoginName, loginLogModel.getLoginName());
        }
        if (StringUtils.isNotBlank(loginLogModel.getIp())) {
            queryWrapper.lambda().like(LoginLogModel::getIp, loginLogModel.getIp());
        }
        queryWrapper.lambda().orderByDesc(LoginLogModel::getCreateDate);
        iPage = loginLogService.page(iPage, queryWrapper);
        return page2LayTablePage(iPage);
    }

    @GetMapping("/view")
    @CacheLock(prefix = "view")
    @ExceptionHandler(ViewException.class)
    @OperateLog(operateModule = "登录日志", operateType = OperateConstant.VIEW, operateDesc = "查看登录日志信息")
    public String view(String id, ModelMap modelMap) {
        LoginLogModel loginLogModel = loginLogService.getById(id);
        modelMap.put("loginLogModel", loginLogModel);
        return PREFIX + VIEW;
    }


    /**
    * 批量删除
    *
    * @param ids id集合
    */
    @ResponseBody
    @PostMapping("delete")
    @CacheLock(prefix = "loginLog-delete")
    @OperateLog(operateModule = "登录日志", operateType = OperateConstant.DELETE, operateDesc = "删除登录日志，可以批量删除")
    public JSONObject delete(String ids) {
        JSONObject jsonObject = new JSONObject();
        if (StringUtils.isEmpty(ids)) {
            jsonObject.put(CommonConstant.AjaxCode.CODE, CommonConstant.AjaxCode.FAIL_CODE);
            jsonObject.put(CommonConstant.AjaxCode.MESSAGE, "请至少选择一条数据！");
            return jsonObject;
        }
        try {
            loginLogService.removeByIds(Arrays.asList(ids.split(CommonConstant.DOC_FLAG)));
            jsonObject.put(CommonConstant.AjaxCode.CODE, CommonConstant.AjaxCode.SUCCESS_CODE);
            jsonObject.put(CommonConstant.AjaxCode.MESSAGE, "删除登录日志数据成功！");
        } catch (Exception e) {
            log.error("删除失败！原因： {}", e.getMessage());
            jsonObject.put(CommonConstant.AjaxCode.CODE, CommonConstant.AjaxCode.FAIL_CODE);
            jsonObject.put(CommonConstant.AjaxCode.MESSAGE, "删除登录日志数据失败！");
        }

        return jsonObject;
    }
}
