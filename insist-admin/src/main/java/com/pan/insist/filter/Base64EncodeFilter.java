package com.pan.insist.filter;

import lombok.extern.log4j.Log4j2;
import org.springframework.core.annotation.Order;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * @author kaiji
 */
@Log4j2
@WebFilter(filterName = "base64EncodeFilter", urlPatterns = "/*",
	initParams = { @WebInitParam(name = "mode", value = "SAMEORIGIN") })
@Order(4)
public class Base64EncodeFilter implements Filter{

	@Override
	public void init(FilterConfig filterConfig) {
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		chain.doFilter(new Base64EncodeParams((HttpServletRequest) request), response);
	}

	@Override
	public void destroy() {
		log.info("销毁BASE64ENCODE对象！");
	}

}
