package com.pan.insist.filter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

/**
 * base64位解密
 * @author kaiji
 * @since 2019-08-15
 */
public class Base64EncodeParams  extends HttpServletRequestWrapper {

	private HttpServletRequest orgRequest;
	
	Base64EncodeParams(HttpServletRequest request) {
		super(request);
		orgRequest = request;
	}

	/**
	 * 获取最原始的request
	 * 
	 * @return
	 */
	public HttpServletRequest getOrgRequest() {
		return orgRequest;
	}

	/**
	 * 获取最原始的request的静态方法
	 * 
	 * @return
	 */
	public static HttpServletRequest getOrgRequest(HttpServletRequest req) {
		if (req instanceof XssAndSqlHttpServletRequestWrapper) {
			return ((XssAndSqlHttpServletRequestWrapper) req).getOrgRequest();
		}

		return req;
	}

	@Override
	public String getParameter(String name) {
		return super.getParameter(name);
	}
	
	@Override
	public String[] getParameterValues(String name) {
		String[] values = super.getParameterValues(name);
		if (null == values || values.length <= 0) {
			return null;
		}
		return values;
	}
}
