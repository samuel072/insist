package com.pan.insist.base;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;

/**
 * 基础控制层
 *
 * @author kaiji
 * @since 2019-12-15
 */
public abstract class BaseController {
	
	protected static final String LIST = "list";
	protected static final String INPUT = "input";
	protected static final String VIEW = "view";
	protected static final String REDIRECT = "redirect:/";

	protected JSONObject page2LayTablePage(IPage<?> iPage) {
		JSONObject json = new JSONObject();
		json.put("code", 0);
		json.put("msg", "查询成功！");
		json.put("count", iPage.getTotal());
		json.put("data", iPage.getRecords());
		return json;
	}
}
