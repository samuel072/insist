package com.pan.insist.config;

import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * <p>
 *  rabbitmq配置
 * </p>
 *
 * @author kaiji
 * @since 2020/1/30 17:34
 */
@Configuration
public class RabbitMqConfig {

    /**
     * 默认是使用jdk来序列化文本消息的， 更改为json的方式
     */
    @Bean
    public MessageConverter messageConverter() {
        return new Jackson2JsonMessageConverter();
    }
}
