package com.pan.insist.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * 配置接口文档
 *
 * @author kaiji
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.pan.insist.controller"))
                // .paths(PathSelectors.regex("/rest/*"))  这个包下的 以rest开头的都会自动生成接口文档
                // 这个包下的 所有的地址都会生成接口文档
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("坚持快速发育API")
                .description("insist接口API")
                .termsOfServiceUrl("http://localhost/")
                .contact(new Contact("kaiJi", "http://localhost/insist", "han_wuhan@sina.com"))
                .version("1.0")
                .build();
    }
}
