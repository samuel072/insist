package com.pan.insist.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

/**
 * <p>
 *  webSocket 配置
 * </p>
 *
 * @author kaiji
 * @since 2020/2/7 21:12
 */
@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig implements WebSocketMessageBrokerConfigurer {

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint("/gs-guide-websocket").setAllowedOrigins("*").withSockJS();

    }

    @Override
    public void configureMessageBroker(MessageBrokerRegistry registry) {
        // 将消息发布到 topic 或者 queue 中
        registry.enableSimpleBroker("/topic", "/queue");
        // 以STOMP开头的STOMP消息/app将路由到 类中的@MessageMapping方法@Controller。
        registry.setApplicationDestinationPrefixes("/app");

    }
}
