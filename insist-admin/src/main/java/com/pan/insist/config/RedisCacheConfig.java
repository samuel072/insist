package com.pan.insist.config;

import com.alibaba.fastjson.parser.ParserConfig;
import com.pan.insist.cache.redis.GenericFastJson2JsonRedisSerializer;
import com.pan.insist.cache.redis.StringRedisSerializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * <p>
 *     redis 缓存配置
 * </p>
 *
 * @author kaiji
 * @since 2019/12/11 20:56
 */
@Configuration
public class RedisCacheConfig {

    @Bean
    public RedisTemplate<String, Object> redisTemplate(LettuceConnectionFactory redisConnectionFactory) {
        RedisTemplate<String, Object> redisTemplate = new RedisTemplate<>();
        redisTemplate.setConnectionFactory(redisConnectionFactory);

        GenericFastJson2JsonRedisSerializer<Object> fastJsonRedisSerializer = new GenericFastJson2JsonRedisSerializer<>();
        // fastjson在2017年3月爆出了在1.2.24以及之前版本存在远程代码执行高危安全漏洞。
        // 所以要使用ParserConfig.getGlobalInstance().addAccept("com.xiaolyuh.");指定序列化白名单
        // ****  划重点 一定要指定白名单 不然会出现异常：
        // autoType is not support. com.pan.insist.model.UserModel****
        // ParserConfig.getGlobalInstance().addAccept("com.pan.insist.model");
        ParserConfig.getGlobalInstance().setAutoTypeSupport(true);
        ParserConfig.getGlobalInstance().addAccept("com.pan.insist.model");

        // 设置值（value）的序列化采用FastJsonRedisSerializer。
        redisTemplate.setValueSerializer(fastJsonRedisSerializer);
        redisTemplate.setHashValueSerializer(fastJsonRedisSerializer);
        // 设置键（key）的序列化采用StringRedisSerializer。
        redisTemplate.setKeySerializer(new StringRedisSerializer());
        redisTemplate.setHashKeySerializer(new StringRedisSerializer());

        redisTemplate.afterPropertiesSet();
        return redisTemplate;
    }
}
