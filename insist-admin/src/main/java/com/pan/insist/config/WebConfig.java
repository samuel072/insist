package com.pan.insist.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * 自定义配置文件 Web-Config
 *
 * @author kaiji
 */

@Data
@Component
@PropertySource(value = "classpath:/my-config.properties")
@ConfigurationProperties(prefix = "my-config")
public class WebConfig {
	/** 环境配置 */
	private String environment;
	/** 系统地址 */
	private String systemUrl;
	/** Host地址 */
	private String host;
	/** FTP地址 */
	private String ftp;
	/** 图片上传路径 */
	private String uploadPath;
	/** 图片展示路径 */
	private String viewPath;
}
