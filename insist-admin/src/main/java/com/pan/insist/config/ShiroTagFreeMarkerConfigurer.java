package com.pan.insist.config;

import com.jagregory.shiro.freemarker.ShiroTags;
import freemarker.template.Configuration;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.freemarker.FreeMarkerViewResolver;

import javax.annotation.Resource;

/**
 * @author kaiji
 */
@Component
public class ShiroTagFreeMarkerConfigurer implements InitializingBean {

    @Resource
    private Configuration configuration;
    @Resource
    private FreeMarkerViewResolver freeMarkerViewResolver;

    @Override
    public void afterPropertiesSet() throws Exception {
        configuration.setSharedVariable("shiro", new ShiroTags());
        //防止页面输出数字,变成2,000
        configuration.setNumberFormat("#");
        // 加上这句后，可以在页面上用${request.contextPath}获取contextPath
        freeMarkerViewResolver.setRequestContextAttribute("request");
    }
}
