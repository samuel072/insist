package com.pan.insist.aop;

import com.alibaba.fastjson.JSON;
import com.pan.insist.annotation.OperateLog;
import com.pan.insist.constant.CommonConstant;
import com.pan.insist.exception.ValidateException;
import com.pan.insist.model.ExceptionLogModel;
import com.pan.insist.model.OperateLogModel;
import com.pan.insist.model.UserModel;
import com.pan.insist.service.ExceptionLogService;
import com.pan.insist.service.OperateLogService;
import com.pan.insist.util.BaseUtil;
import com.pan.insist.util.IpUtils;
import lombok.extern.log4j.Log4j2;
import org.apache.shiro.SecurityUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.ui.ModelMap;
import org.springframework.validation.support.BindingAwareModelMap;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * 切面处理类，操作日志异常日志记录处理
 *
 * @author kaiji
 * @since 2020-01-20 10:02:03
 */
@Log4j2
@Aspect
@Order(2)
@Component
public class OperateLogAop {


    @Resource
    private OperateLogService operateLogService;
    @Resource
    private ExceptionLogService exceptionLogService;


    /**
     * 设置操作日志切入点 记录操作日志 在注解的位置切入代码
     */
    @Pointcut("@annotation(com.pan.insist.annotation.OperateLog)")
    public void operateLogPointCut() {
    }

    /**
     * 设置操作异常切入点记录异常日志 扫描所有类包下操作
     */
    @Pointcut("execution(* com.pan.insist..*.*(..))")
    public void operateExceptionLogPointCut() {
    }

    /**
     * 正常返回通知，拦截用户操作日志，连接点正常执行完成后执行， 如果连接点抛出异常，则不会执行
     *
     * @param joinPoint 切入点
     * @param keys      返回结果
     */
    @AfterReturning(value = "operateLogPointCut()", returning = "keys")
    public void saveOperateLog(JoinPoint joinPoint, Object keys) {
        HttpServletRequest request = BaseUtil.getRequest();

        try {
            OperateLogModel operateLogModel = new OperateLogModel();

            // 从切面织入点处通过反射机制获取织入点处的方法
            MethodSignature signature = (MethodSignature) joinPoint.getSignature();
            // 获取切入点所在的方法
            Method method = signature.getMethod();
            // 获取操作
            OperateLog opLog = method.getAnnotation(OperateLog.class);
            if (opLog != null) {
                operateLogModel.setModule(opLog.operateModule());
                operateLogModel.setType(opLog.operateType());
                operateLogModel.setDescription(opLog.operateDesc());
            }
            // 获取请求的类名
            String className = joinPoint.getTarget().getClass().getName();
            // 获取请求的方法名
            String methodName = method.getName();
            methodName = className + CommonConstant.SPOT + methodName;
            // 请求方法
            operateLogModel.setMethod(methodName);

            // 请求的参数
            Map<String, String> rtnMap = convertMap(request.getParameterMap());
            // 将参数所在的数组转换成json
            String params = JSON.toJSONString(rtnMap);
            operateLogModel.setRequestParams(params);
            // 判断是否是ajax
            ResponseBody responseBody = method.getAnnotation(ResponseBody.class);
            if (null == responseBody) {
                // 返回modelAndView
                String responseParams = getModelMapParams(joinPoint);
                operateLogModel.setResponseParams(responseParams);
            } else {
                operateLogModel.setResponseParams(JSON.toJSONString(keys));
            }

            UserModel userModel = (UserModel) SecurityUtils.getSubject().getPrincipal();
            operateLogModel.setUserId(userModel.getId());
            operateLogModel.setUserName(userModel.getUsername());
            operateLogModel.setIp(IpUtils.getIpFromRequest(request));
            operateLogModel.setUrl(request.getRequestURI());
            operateLogModel.setCreateDate(LocalDateTime.now());
            operateLogService.save(operateLogModel);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 异常返回通知，用于拦截异常日志信息 连接点抛出异常后执行
     *
     * @param joinPoint 切入点
     * @param e         异常信息
     */
    @AfterThrowing(pointcut = "operateExceptionLogPointCut()", throwing = "e")
    public void saveExceptionLog(JoinPoint joinPoint, Throwable e) {
        try {
            if (e instanceof ValidateException) {
                return;
            }
            ExceptionLogModel exceptionLogModel = new ExceptionLogModel();
            // 从切面织入点处通过反射机制获取织入点处的方法
            MethodSignature signature = (MethodSignature) joinPoint.getSignature();
            // 获取切入点所在的方法
            Method method = signature.getMethod();
            // 获取请求的类名
            String className = joinPoint.getTarget().getClass().getName();
            // 获取请求的方法名
            String methodName = method.getName();
            methodName = className + CommonConstant.SPOT + methodName;

            exceptionLogModel.setMethod(methodName);
            // 异常名称
            exceptionLogModel.setExceptionName(e.getClass().getName());
            // 异常信息
            exceptionLogModel.setMessage(stackTraceToString(e.getClass().getName(), e.getMessage(), e.getStackTrace()));
            UserModel userModel = (UserModel) SecurityUtils.getSubject().getPrincipal();
            if (null == userModel) {
                exceptionLogModel.setUserId("000000000000");
                exceptionLogModel.setUserName("佚名");
            } else {
                exceptionLogModel.setUserId(userModel.getId());
                exceptionLogModel.setUserName(userModel.getUsername());
            }

            // 发生异常时间
            exceptionLogModel.setCreateDate(LocalDateTime.now());
            exceptionLogService.save(exceptionLogModel);

        } catch (Exception e2) {
            log.error("保存异常日志错误。原因：", e2);
        }

    }

    /**
     * 转换request 请求参数
     *
     * @param paramMap request获取的参数数组
     */
    private Map<String, String> convertMap(Map<String, String[]> paramMap) {
        Map<String, String> rtnMap = new HashMap<>(CommonConstant.DEFAULT_INITIAL_CAPACITY);
        for (String key : paramMap.keySet()) {
            rtnMap.put(key, paramMap.get(key)[0]);
        }
        return rtnMap;
    }

    /**
     * 转换异常信息为字符串
     *
     * @param exceptionName    异常名称
     * @param exceptionMessage 异常信息
     * @param elements         堆栈信息
     */
    private String stackTraceToString(String exceptionName, String exceptionMessage, StackTraceElement[] elements) {
        StringBuilder stringBuilder = new StringBuilder();
        for (StackTraceElement stet : elements) {
            stringBuilder.append(stet).append("\n");
        }
        return exceptionName + CommonConstant.COLON_FLAG + exceptionMessage + "\n\t" + stringBuilder;
    }

    /**
     * 获取modelMap里面的值
     *
     * @param joinPoint
     *      切入点
     * @return  modelMap里面的值
     */
    private String getModelMapParams(JoinPoint joinPoint) {
        Object[] args = joinPoint.getArgs();

        List<ModelMap> modelList = Arrays.stream(args)
                .filter(BindingAwareModelMap.class::isInstance)
                .map(ModelMap.class::cast)
                .collect(Collectors.toList());

        StringBuilder stringBuilder = new StringBuilder();
        for (ModelMap modelMap : modelList) {
            stringBuilder.append(parseModelMap(modelMap));
        }
        String str = JSON.toJSONString(stringBuilder.toString());
        String regex = "\\\\+";
        str = str.replaceAll(regex, "\\\\");
        return str;
    }

    /**
     * 解析Map
     *
     * @param modelMap
     *      modelMap里面的参数
     * @return  方法的返回值
     */
    private StringBuilder parseModelMap(ModelMap modelMap) {
        StringBuilder stringBuilder = new StringBuilder();
        modelMap.forEach((key, value)-> {
            String params = JSON.toJSONString(value);
            stringBuilder.append(params);
        });
        return stringBuilder;
    }
}
