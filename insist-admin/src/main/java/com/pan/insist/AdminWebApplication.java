package com.pan.insist;

import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

/**
 * 启动类
 * <p>
 *  SpringBootServletInitializer的作用：</br>
 * 	我们需要类似于web.xml的配置方式来启动spring上下文了，</br>
 *  在Application类的同级添加一个SpringBootStartApplication类</br>
 *  只在打包成war包时，使用外部容器时才需要继承这个类重写配置类的资源。其他的时候不需要。</br>
 * </p>
 * ServletComponentScan 扫描的作用：Filter lister  servlet 扫描<br/>
 * EnableRedisHttpSession : spring-redis-session<br/>
 * EnableCaching		: 开启缓存
 * EnableRabbit 		: 开启rabbitmq
 *
 * @author kaiji
 */
@EnableRabbit
@ServletComponentScan
@SpringBootApplication
@EnableRedisHttpSession
public class AdminWebApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		/*// 第一种启动方式
	 	SpringApplication.run(AdminWebApplication.class, args);*/

		// 第二种启动方式
		SpringApplication app = new SpringApplication(AdminWebApplication.class);
		app.setBannerMode(Banner.Mode.OFF);
		app.run(args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(AdminWebApplication.class);
	}

	
}
