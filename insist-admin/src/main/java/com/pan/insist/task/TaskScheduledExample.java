package com.pan.insist.task;

import lombok.extern.log4j.Log4j2;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * <p>
 *  Scheduled 注解版本任务示例
 * </p>
 *
 * @author kaiji
 * @since 2020/2/7 10:50
 */
@Log4j2
@Component
public class TaskScheduledExample {

    /**
     * Cron表达式参数分别表示：
     * 秒（0~59） 例如0/5表示每5秒
     * 分（0~59）
     * 时（0~23）
     * 月的某天（0~31） 需计算
     * 月（0~11）
     * 周几（ 可填1-7 或 SUN/MON/TUE/WED/THU/FRI/SAT）
     */
    @Scheduled(cron = "0 0/30 * * * ?")
    protected void taskRun() {
       log.info("注解版本的任务调度！");
    }
}
