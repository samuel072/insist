layui.config({
    base: Insist.ctx + '/static/plugins/layuiadmin/' //静态资源所在路径
}).extend({
    index: 'lib/index' //主入口模块
}).use(['index', 'form', 'table', 'setter'], function () {
    var table = layui.table;
    var config = layui.setter;
    var form = layui.form;

    var manageRole = {
        tableId: "roleTable"
    };

    manageRole.initColumn = function () {
        return [[
            {type: 'checkbox'}
            , {field: 'name', title: '角色名'}
            , {field: 'code', title: '代码', edit: 'text'}
            , {fixed: 'right', title: '操作', toolbar: '#tableRightBar', width: 200}
        ]];
    };

    // 查询条件
    manageRole.search = function (data) {
        table.reload(manageRole.tableId, {where: data});
    };

    // 搜索
    form.on("submit(LAY-search)", function (data) {
        manageRole.search(data.field);
    });

    manageRole.view = function (data) {
        var pageIndex = layer.open({
            type: 2
            , title: '查看角色'
            , content: Insist.ctx + '/role/view?id=' + data.id
            , maxmin: true
            , btn: ['返回']
            , yes: function (index) {
                layer.close(index);
            }
        });
        layer.full(pageIndex);
    };

    manageRole.edit = function (data) {
        Insist.commonFunction(table,'/role/update?id=' + data.id, manageRole.tableId);
    };

    manageRole.add = function() {
        Insist.commonFunction(table,'/role/save', manageRole.tableId);
    };

    manageRole.delete = function (data) {
        Insist.commonDel(table, "/role/delete", manageRole.tableId, data);
    };

    manageRole.roleConfig = function(data) {
        Insist.commonFunction(table, "/role/roleConfig?id="+data.id, manageRole.tableId, data);
    };

    // 监听头部事件
    table.on('toolbar(' + manageRole.tableId + ')', function (obj) {
        var checkStatus = table.checkStatus(obj.config.id);
        var data = checkStatus.data;
        switch (obj.event) {
            case 'del':
                manageRole.delete(data);
                break;
            case 'add':
                manageRole.add();
                break;
        }
    });

    // 监听行工具事件
    table.on('tool(' + manageRole.tableId + ')', function (obj) {
        var data = obj.data;
        switch (obj.event) {
            case 'view':
                manageRole.view(data);
                break;
            case 'edit':
                manageRole.edit(data);
                break;
            case 'roleConfig':
                manageRole.roleConfig(data);
                break;
        }
    });


    table.render({
        elem: '#' + manageRole.tableId
        , url: Insist.ctx + '/role/listJson'
        , toolbar: '#tableHeadBar'
        , title: '角色数据表'
        , cellMinWidth: 80
        , cols: manageRole.initColumn(),
        page: config.page,
        request: config.request
    });

});