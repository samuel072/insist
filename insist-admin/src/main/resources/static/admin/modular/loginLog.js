layui.config({
    base: Insist.ctx + '/static/plugins/layuiadmin/' //静态资源所在路径
}).extend({
    index: 'lib/index' //主入口模块
}).use(['index', 'form', 'table', 'setter', 'util'], function () {
    var table = layui.table;
    var config = layui.setter;
    var form = layui.form;
    var util = layui.util;

    var manageLoginLog = {
        tableId: "loginLogTable",
        condition: {
            loginName: "",
            ip: ""
        }
    };

    manageLoginLog.initColumn = function () {
        return [[
            {type: 'checkbox'},
            {title: '登录用户', field: 'loginName', visible: true, align: 'center', valign: 'middle'},
            {title: '日志名称', field: 'logName', visible: true, align: 'center', valign: 'middle'},
            {title: 'ip地址', field: 'ip', visible: true, align: 'center', valign: 'middle'},
            {title: '创建时间', field: 'createDate', visible: true, align: 'center', valign: 'middle', templet: function (res) {
                    if (res.createDate == null) {
                        return "";
                    } else {
                        return util.toDateString(res.createDate, "yyyy-MM-dd HH:mm:ss")
                    }
                }
            },
            {fixed: 'right', title: '操作', toolbar: '#tableRightBar', width: 80}
        ]];
    };

    // 查询条件
    manageLoginLog.search = function (data) {
        table.reload(manageLoginLog.tableId, {where: data});
    };

    // 搜索
    form.on("submit(LAY-search)", function (data) {
        manageLoginLog.search(data.field);
    });

    manageLoginLog.view = function (data) {
        var pageIndex = layer.open({
            type: 2
            , title: '查看登录日志'
            , content: Insist.ctx + '/loginLog/view?id=' + data.id
            , maxmin: true
            , btn: ['返回']
            , yes: function (index) {
                layer.close(index);
            }
        });
        layer.full(pageIndex);
    };

    manageLoginLog.edit = function (data) {
        Insist.commonFunction(table,'/loginLog/update?id=' + data.id, manageLoginLog.tableId);
    };

    manageLoginLog.add = function() {
        Insist.commonFunction(table,'/loginLog/save', manageLoginLog.tableId);
    };

    manageLoginLog.delete = function (data) {
        Insist.commonDel(table, "/loginLog/delete", manageLoginLog.tableId, data);
    };

    // 监听头部事件
    table.on('toolbar(' + manageLoginLog.tableId + ')', function (obj) {
        var checkStatus = table.checkStatus(obj.config.id);
        var data = checkStatus.data;
        switch (obj.event) {
            case 'del':
                manageLoginLog.delete(data);
                break;
            case 'add':
                manageLoginLog.add();
                break;
        }
    });
    // 监听行工具事件
    table.on('tool(' + manageLoginLog.tableId + ')', function (obj) {
        var data = obj.data;
        switch (obj.event) {
            case 'view':
                manageLoginLog.view(data);
                break;
            case 'edit':
                manageLoginLog.edit(data);
                break;
        }
    });


    table.render({
        elem: '#' + manageLoginLog.tableId
        , url: Insist.ctx + '/loginLog/listJson'
        , toolbar: '#tableHeadBar'
        , title: '登录日志数据表'
        , cellMinWidth: 80
        , cols: manageLoginLog.initColumn(),
        page: config.page,
        request: config.request
    });

});
