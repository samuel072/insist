layui.config({
    base: Insist.ctx + '/static/plugins/layuiadmin/' //静态资源所在路径
}).extend({
    index: 'lib/index' //主入口模块
}).use(['index', 'form', 'table', 'setter'], function () {
    var table = layui.table;
    var config = layui.setter;
    var form = layui.form;

    var manageImages = {
        tableId: "imagesTable",
        condition: {
            username: ""
        }
    };

    manageImages.initColumn = function () {
        return [[
            {type: 'checkbox'},
            {title: '图片地址', field: 'url', visible: true, align: 'center', valign: 'middle'},
            {title: '1: 未删除 2：删除', field: 'deletion', visible: true, align: 'center', valign: 'middle'},
            {fixed: 'right', title: '操作', toolbar: '#tableRightBar', width: 120}
        ]];
    };

    // 查询条件
    manageImages.search = function (data) {
        table.reload(manageImages.tableId, {where: data});
    };

    // 搜索
    form.on("submit(LAY-search)", function (data) {
        manageImages.search(data.field);
    });

    manageImages.view = function (data) {
        var pageIndex = layer.open({
            type: 2
            , title: '查看图片'
            , content: Insist.ctx + '/images/view?id=' + data.id
            , maxmin: true
            , btn: ['返回']
            , yes: function (index) {
                layer.close(index);
            }
        });
        layer.full(pageIndex);
    };

    manageImages.edit = function (data) {
        Insist.commonFunction(table,'/images/update?id=' + data.id, manageImages.tableId);
    };

    manageImages.add = function() {
        Insist.commonFunction(table,'/images/save', manageImages.tableId);
    };

    manageImages.delete = function (data) {
        Insist.commonDel(table, "/images/delete", manageImages.tableId, data);
    };

    // 监听头部事件
    table.on('toolbar(' + manageImages.tableId + ')', function (obj) {
        var checkStatus = table.checkStatus(obj.config.id);
        var data = checkStatus.data;
        switch (obj.event) {
            case 'del':
                manageImages.delete(data);
                break;
            case 'add':
                manageImages.add();
                break;
        }
    });
    // 监听行工具事件
    table.on('tool(' + manageImages.tableId + ')', function (obj) {
        var data = obj.data;
        switch (obj.event) {
            case 'view':
                manageImages.view(data);
                break;
            case 'edit':
                manageImages.edit(data);
                break;
        }
    });


    table.render({
        elem: '#' + manageImages.tableId
        , url: Insist.ctx + '/images/listJson'
        , toolbar: '#tableHeadBar'
        , title: '图片数据表'
        , cellMinWidth: 80
        , cols: manageImages.initColumn(),
        page: config.page,
        request: config.request
    });

});