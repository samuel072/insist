layui.config({
    base: Insist.ctx + '/static/plugins/layuiadmin/' //静态资源所在路径
}).extend({
    index: 'lib/index', //主入口模块
    treetable: 'treetable-lay/treetable'
}).use(['index', 'form', 'table', 'setter', 'treetable'], function () {
    var table = layui.table;
    var config = layui.setter;
    var form = layui.form;
    var treetable = layui.treetable;

    var manageMenu = {
        tableId: "menuTable"
    };

    manageMenu.initColumn = function () {
        return [[
            {type: 'checkbox'}
            , {field: 'name', title: '菜单名'}
            , {field: 'url', title: '菜单地址'}
            , {field: 'pidName', title: '上级菜单'}
            , {fixed: 'right', title: '操作', toolbar: '#tableRightBar', width: 120}
        ]];
    };

    // 查询条件
    manageMenu.search = function (data) {
        // treetable.reload(manageMenu.tableId, {where: data});
        treetable.reload({params:{updateTreeDataCache:false},where:{keyword:data.name}});
    };

    // 搜索
    form.on("submit(LAY-search)", function (data) {
        manageMenu.search(data.field);
    });

    manageMenu.view = function (data) {
        var pageIndex = layer.open({
            type: 2
            , title: '查看菜单'
            , content: Insist.ctx + '/menu/view?id=' + data.id
            , maxmin: true
            , btn: ['返回']
            , yes: function (index) {
                layer.close(index);
            }
        });
        layer.full(pageIndex);
    };

    manageMenu.edit = function (data) {
        Insist.commonFunction(treetable,'/menu/update?id=' + data.id, manageMenu.tableId);
    };

    manageMenu.add = function() {
        Insist.commonFunction(treetable,'/menu/save', manageMenu.tableId);
    };

    manageMenu.delete = function (data) {
        Insist.commonDel(treetable, "/menu/delete", manageMenu.tableId, data);
    };

    // 监听头部事件
    table.on('toolbar(' + manageMenu.tableId + ')', function (obj) {
        var checkStatus = table.checkStatus(obj.config.id);
        var data = checkStatus.data;
        switch (obj.event) {
            case 'del':
                manageMenu.delete(data);
                break;
            case 'add':
                manageMenu.add();
                break;
        }
    });

    // 监听行工具事件
    table.on('tool(' + manageMenu.tableId + ')', function (obj) {
        var data = obj.data;
        switch (obj.event) {
            case 'view':
                manageMenu.view(data);
                break;
            case 'edit':
                manageMenu.edit(data);
                break;
        }
    });

    manageMenu.initTable = function() {
        return treetable.render({
            elem: '#' + manageMenu.tableId,
            url: Insist.ctx + '/menu/listJson',
            toolbar: '#tableHeadBar',
            page: config.page,
            request: config.request,
            height: "full-98",
            cellMinWidth: 100,
            cols: manageMenu.initColumn(),
            //树形图标显示在第几列
            treeColIndex: 1,
            //最上级的父级id
            treeSpid: "0",
            // id字段的名称
            treeIdName: 'id',
            // pid字段的名称
            treePidName: 'pid',
            //是否默认折叠
            treeDefaultClose: false,
            //父级展开时是否自动展开所有子级
            treeLinkage: true
        });
    };

    var tableResult = manageMenu.initTable();

});