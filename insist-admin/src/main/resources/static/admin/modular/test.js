layui.config({
    base: Insist.ctx + '/static/plugins/layuiadmin/' //静态资源所在路径
}).extend({
    index: 'lib/index' //主入口模块
}).use(['index', 'form', 'table', 'setter'], function () {
    var table = layui.table;
    var config = layui.setter;
    var form = layui.form;

    var manageTest = {
        tableId: "testTable",
        condition: {
            username: ""
        }
    };

    manageTest.initColumn = function () {
        return [[
            {type: 'checkbox'},
            {title: '测试人', field: 'testName', visible: true, align: 'center', valign: 'middle'},
            {title: '测试事件', field: 'testEvent', visible: true, align: 'center', valign: 'middle'},
            {title: '修改时间', field: 'modityDate', visible: true, align: 'center', valign: 'middle'},
            {fixed: 'right', title: '操作', toolbar: '#tableRightBar', width: 120}
        ]];
    };

    // 查询条件
    manageTest.search = function (data) {
        table.reload(manageTest.tableId, {where: data});
    };

    // 搜索
    form.on("submit(LAY-search)", function (data) {
        manageTest.search(data.field);
    });

    manageTest.view = function (data) {
        var pageIndex = layer.open({
            type: 2
            , title: '查看测试'
            , content: Insist.ctx + '/test/view?id=' + data.id
            , maxmin: true
            , btn: ['返回']
            , yes: function (index) {
                layer.close(index);
            }
        });
        layer.full(pageIndex);
    };

    manageTest.edit = function (data) {
        Insist.commonFunction(table,'/test/update?id=' + data.id, manageTest.tableId);
    };

    manageTest.add = function() {
        Insist.commonFunction(table,'/test/save', manageTest.tableId);
    };

    manageTest.delete = function (data) {
        Insist.commonDel(table, "/test/delete", manageTest.tableId, data);
    };

    // 监听头部事件
    table.on('toolbar(' + manageTest.tableId + ')', function (obj) {
        var checkStatus = table.checkStatus(obj.config.id);
        var data = checkStatus.data;
        switch (obj.event) {
            case 'del':
                manageTest.delete(data);
                break;
            case 'add':
                manageTest.add();
                break;
        }
    });
    // 监听行工具事件
    table.on('tool(' + manageTest.tableId + ')', function (obj) {
        var data = obj.data;
        switch (obj.event) {
            case 'view':
                manageTest.view(data);
                break;
            case 'edit':
                manageTest.edit(data);
                break;
        }
    });


    table.render({
        elem: '#' + manageTest.tableId
        , url: Insist.ctx + '/test/listJson'
        , toolbar: '#tableHeadBar'
        , title: '测试数据表'
        , cellMinWidth: 80
        , cols: manageTest.initColumn(),
        page: config.page,
        request: config.request
    });

});