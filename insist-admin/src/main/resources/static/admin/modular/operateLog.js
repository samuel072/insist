layui.config({
    base: Insist.ctx + '/static/plugins/layuiadmin/' //静态资源所在路径
}).extend({
    index: 'lib/index' //主入口模块
}).use(['index', 'form', 'table', 'setter'], function () {
    var table = layui.table;
    var config = layui.setter;
    var form = layui.form;

    var manageOperateLog = {
        tableId: "operateLogTable",
        condition: {
            userName: "",
            ip: "",
            type: "",
            method: "",
            module: ""
        }
    };

    manageOperateLog.initColumn = function () {
        return [[
            {type: 'checkbox'},
            {title: '操作模块', field: 'module', visible: true, align: 'center', valign: 'middle'},
            {title: '操作类型', field: 'type', visible: true, align: 'center', valign: 'middle'},
            {title: '操作描述', field: 'description', visible: true, align: 'center', valign: 'middle'},
            {title: '请求参数', field: 'requestParams', visible: true, align: 'center', valign: 'middle'},
            {title: '操作员名称', field: 'userName', visible: true, align: 'center', valign: 'middle'},
            {title: '操作方法', field: 'method', visible: true, align: 'center', valign: 'middle'},
            {title: '访问地址', field: 'url', visible: true, align: 'center', valign: 'middle'},
            {title: '访问IP', field: 'ip', visible: true, align: 'center', valign: 'middle'},
            {title: '创建时间', field: 'createDate', visible: true, align: 'center', valign: 'middle'},
            {fixed: 'right', title: '操作', toolbar: '#tableRightBar', width: 80}
        ]];
    };

    // 查询条件
    manageOperateLog.search = function (data) {
        table.reload(manageOperateLog.tableId, {where: data});
    };

    // 搜索
    form.on("submit(LAY-search)", function (data) {
        manageOperateLog.search(data.field);
    });

    manageOperateLog.view = function (data) {
        var pageIndex = layer.open({
            type: 2
            , title: '查看操作日志'
            , content: Insist.ctx + '/operateLog/view?id=' + data.id
            , maxmin: true
            , btn: ['返回']
            , yes: function (index) {
                layer.close(index);
            }
        });
        layer.full(pageIndex);
    };

    manageOperateLog.edit = function (data) {
        Insist.commonFunction(table,'/operateLog/update?id=' + data.id, manageOperateLog.tableId);
    };

    manageOperateLog.add = function() {
        Insist.commonFunction(table,'/operateLog/save', manageOperateLog.tableId);
    };

    manageOperateLog.delete = function (data) {
        Insist.commonDel(table, "/operateLog/delete", manageOperateLog.tableId, data);
    };

    // 监听头部事件
    table.on('toolbar(' + manageOperateLog.tableId + ')', function (obj) {
        var checkStatus = table.checkStatus(obj.config.id);
        var data = checkStatus.data;
        switch (obj.event) {
            case 'del':
                manageOperateLog.delete(data);
                break;
            case 'add':
                manageOperateLog.add();
                break;
        }
    });
    // 监听行工具事件
    table.on('tool(' + manageOperateLog.tableId + ')', function (obj) {
        var data = obj.data;
        switch (obj.event) {
            case 'view':
                manageOperateLog.view(data);
                break;
            case 'edit':
                manageOperateLog.edit(data);
                break;
        }
    });


    table.render({
        elem: '#' + manageOperateLog.tableId
        , url: Insist.ctx + '/operateLog/listJson'
        , toolbar: '#tableHeadBar'
        , title: '操作日志数据表'
        , cellMinWidth: 80
        , cols: manageOperateLog.initColumn(),
        page: config.page,
        request: config.request
    });

});