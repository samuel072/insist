layui.config({
    base: Insist.ctx + '/static/plugins/layuiadmin/' //静态资源所在路径
}).extend({
    index: 'lib/index' //主入口模块
}).use(['index', 'form', 'table', 'setter'], function () {
    var table = layui.table;
    var config = layui.setter;
    var form = layui.form;

    var manageExceptionLog = {
        tableId: "exceptionLogTable",
        condition: {
            exceptionName: "",
            ip: "",
            method: ""
        }
    };

    manageExceptionLog.initColumn = function () {
        return [[
            {type: 'checkbox'},
            {title: '异常名称', field: 'exceptionName', visible: true, align: 'center', valign: 'middle'},
            {title: '请求参数', field: 'requestParams', visible: true, align: 'center', valign: 'middle'},
            {title: '异常信息', field: 'message', visible: true, align: 'center', valign: 'middle'},
            {title: '操作员名称', field: 'userName', visible: true, align: 'center', valign: 'middle'},
            {title: '访问方法', field: 'method', visible: true, align: 'center', valign: 'middle'},
            {title: '请求地址', field: 'url', visible: true, align: 'center', valign: 'middle'},
            {title: '访问IP', field: 'ip', visible: true, align: 'center', valign: 'middle'},
            {title: '创建时间', field: 'createDate', visible: true, align: 'center', valign: 'middle'},
            {fixed: 'right', title: '操作', toolbar: '#tableRightBar', width: 80}
        ]];
    };

    // 查询条件
    manageExceptionLog.search = function (data) {
        table.reload(manageExceptionLog.tableId, {where: data});
    };

    // 搜索
    form.on("submit(LAY-search)", function (data) {
        manageExceptionLog.search(data.field);
    });

    manageExceptionLog.view = function (data) {
        var pageIndex = layer.open({
            type: 2
            , title: '查看异常日志'
            , content: Insist.ctx + '/exceptionLog/view?id=' + data.id
            , maxmin: true
            , btn: ['返回']
            , yes: function (index) {
                layer.close(index);
            }
        });
        layer.full(pageIndex);
    };

    manageExceptionLog.edit = function (data) {
        Insist.commonFunction(table,'/exceptionLog/update?id=' + data.id, manageExceptionLog.tableId);
    };

    manageExceptionLog.add = function() {
        Insist.commonFunction(table,'/exceptionLog/save', manageExceptionLog.tableId);
    };

    manageExceptionLog.delete = function (data) {
        Insist.commonDel(table, "/exceptionLog/delete", manageExceptionLog.tableId, data);
    };

    // 监听头部事件
    table.on('toolbar(' + manageExceptionLog.tableId + ')', function (obj) {
        var checkStatus = table.checkStatus(obj.config.id);
        var data = checkStatus.data;
        switch (obj.event) {
            case 'del':
                manageExceptionLog.delete(data);
                break;
            case 'add':
                manageExceptionLog.add();
                break;
        }
    });
    // 监听行工具事件
    table.on('tool(' + manageExceptionLog.tableId + ')', function (obj) {
        var data = obj.data;
        switch (obj.event) {
            case 'view':
                manageExceptionLog.view(data);
                break;
            case 'edit':
                manageExceptionLog.edit(data);
                break;
        }
    });


    table.render({
        elem: '#' + manageExceptionLog.tableId
        , url: Insist.ctx + '/exceptionLog/listJson'
        , toolbar: '#tableHeadBar'
        , title: '异常日志数据表'
        , cellMinWidth: 80
        , cols: manageExceptionLog.initColumn(),
        page: config.page,
        request: config.request
    });

});