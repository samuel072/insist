layui.config({
    base: Insist.ctx + '/static/plugins/layuiadmin/' //静态资源所在路径
}).extend({
    index: 'lib/index' //主入口模块
}).use(['index', 'form', 'table', 'setter'], function () {
    var table = layui.table;
    var config = layui.setter;
    var form = layui.form;

    var manageTaskCron = {
        tableId: "taskCronTable",
        condition: {
            name: "",
            code: ""
        }
    };

    manageTaskCron.initColumn = function () {
        return [[
            {type: 'checkbox'},
            {title: '任务名称', field: 'name', visible: true, align: 'center', valign: 'middle'},
            {title: '任务编号', field: 'code', visible: true, align: 'center', valign: 'middle'},
            {title: '任务执行规则', field: 'cron', visible: true, align: 'center', valign: 'middle'},
            {fixed: 'right', title: '操作', toolbar: '#tableRightBar', width: 120}
        ]];
    };

    // 查询条件
    manageTaskCron.search = function (data) {
        table.reload(manageTaskCron.tableId, {where: data});
    };

    // 搜索
    form.on("submit(LAY-search)", function (data) {
        manageTaskCron.search(data.field);
    });

    manageTaskCron.view = function (data) {
        var pageIndex = layer.open({
            type: 2
            , title: '查看任务管理'
            , content: Insist.ctx + '/taskCron/view?id=' + data.id
            , maxmin: true
            , btn: ['返回']
            , yes: function (index) {
                layer.close(index);
            }
        });
        layer.full(pageIndex);
    };

    manageTaskCron.edit = function (data) {
        Insist.commonFunction(table,'/taskCron/update?id=' + data.id, manageTaskCron.tableId);
    };

    manageTaskCron.add = function() {
        Insist.commonFunction(table,'/taskCron/save', manageTaskCron.tableId);
    };

    manageTaskCron.delete = function (data) {
        Insist.commonDel(table, "/taskCron/delete", manageTaskCron.tableId, data);
    };

    // 监听头部事件
    table.on('toolbar(' + manageTaskCron.tableId + ')', function (obj) {
        var checkStatus = table.checkStatus(obj.config.id);
        var data = checkStatus.data;
        switch (obj.event) {
            case 'del':
                manageTaskCron.delete(data);
                break;
            case 'add':
                manageTaskCron.add();
                break;
        }
    });
    // 监听行工具事件
    table.on('tool(' + manageTaskCron.tableId + ')', function (obj) {
        var data = obj.data;
        switch (obj.event) {
            case 'view':
                manageTaskCron.view(data);
                break;
            case 'edit':
                manageTaskCron.edit(data);
                break;
        }
    });


    table.render({
        elem: '#' + manageTaskCron.tableId
        , url: Insist.ctx + '/taskCron/listJson'
        , toolbar: '#tableHeadBar'
        , title: '任务管理数据表'
        , cellMinWidth: 80
        , cols: manageTaskCron.initColumn(),
        page: config.page,
        request: config.request
    });

});