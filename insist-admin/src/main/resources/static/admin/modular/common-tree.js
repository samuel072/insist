layui.use("tree", function () {
    const tree = layui.tree;

    tree.render({
        elem: '#layui-tree',
        data: getTree(),
        isJump: true,
        id: 'menuTreeId',
        click: function (obj) {
            $("#treeId").val(obj.data.id);
            $("#treeName").val(obj.data.title);
            $(".layui-tree-txt").css("background-color", "");
            $(".layui-tree-txt").css("color", "#555");
            obj.elem.find('.layui-tree-txt').first().css("background-color", "#c9c9c9");
            obj.elem.find('.layui-tree-txt').first().css("color", "#fff");
        }
    });

    function getTree() {
        var data = [];
        $.ajax({
            url: Insist.ctx + '/menu/allList',
            type: 'get',
            async: false,
            success: function (res) {
                data = res;
            }
        });
        return data;
    }
});