layui.config({
    base: Insist.ctx + '/static/plugins/layuiadmin/' //静态资源所在路径
}).extend({
    index: 'lib/index' //主入口模块
}).use(['index', 'form', 'table', 'util', 'setter'], function () {
    var table = layui.table;
    var util = layui.util;
    var config = layui.setter;
    var form = layui.form;

    var manageUser = {
        tableId: "userTable",
        condition: {
            username: ""
        }
    };

    manageUser.initColumn = function () {
        return [[
            {type: 'checkbox'}
            , {field: 'username', title: '用户名'}
            , {field: 'phone', title: '手机号', edit: 'text'}
            , {field: 'loginIp', title: '登录IP地址', sort: true}
            , {
                field: 'loginDate', title: '登录时间', edit: 'text', sort: true, templet: function (res) {
                    if (res.loginDate == null) {
                        return "";
                    } else {
                        return util.toDateString(res.loginDate, "yyyy-MM-dd HH:mm:ss")
                    }
                }
            }
            , {
                field: 'status', title: '状态', sort: true, templet: function (res) {
                    if (res.status === 0) {
                        return "启用";
                    } else {
                        return "停用";
                    }
                }
            }
            , {fixed: 'right', title: '操作', toolbar: '#tableRightBar', width: 120}
        ]];
    };

    // 查询条件
    manageUser.search = function (data) {
        table.reload(manageUser.tableId, {where: data});
    };

    // 搜索
    form.on("submit(LAY-search)", function (data) {
        manageUser.search(data.field);
    });

    manageUser.view = function (data) {
        var pageIndex = layer.open({
            type: 2
            , title: '查看用户'
            , content: Insist.ctx + '/user/view?id=' + data.id
            , maxmin: true
            , btn: ['返回']
            , yes: function (index) {
                layer.close(index);
            }
        });
        layer.full(pageIndex);
    };

    manageUser.edit = function (data) {
        Insist.commonFunction(table,'/user/update?id=' + data.id, manageUser.tableId);
    };

    manageUser.add = function() {
        Insist.commonFunction(table,'/user/save', manageUser.tableId);
    };

    manageUser.delete = function (data) {
        Insist.commonDel(table, "/user/delete", manageUser.tableId, data);
    };

    // 监听头部事件
    table.on('toolbar(' + manageUser.tableId + ')', function (obj) {
        var checkStatus = table.checkStatus(obj.config.id);
        var data = checkStatus.data;
        switch (obj.event) {
            case 'del':
                manageUser.delete(data);
                break;
            case 'add':
                manageUser.add();
                break;
        }
    });
    // 监听行工具事件
    table.on('tool(' + manageUser.tableId + ')', function (obj) {
        var data = obj.data;
        switch (obj.event) {
            case 'view':
                manageUser.view(data);
                break;
            case 'edit':
                manageUser.edit(data);
                break;
        }
    });


    table.render({
        elem: '#' + manageUser.tableId
        , url: Insist.ctx + '/user/listJson'
        , toolbar: '#tableHeadBar'
        , title: '用户数据表'
        , cellMinWidth: 80
        , cols: manageUser.initColumn(),
        page: config.page,
        request: config.request
    });

});