<#include "../common/common_ctx.ftl" />
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" type="text/css" href="${ctx}/static/plugins/layuiadmin/layui/css/layui.css"/>
    <title><#if imagesModel?? && imagesModel.id??>编辑<#else>添加</#if>图片详细信息</title>
</head>
<body>

<div class="layui-form" lay-filter="form-submit" style="padding: 20px 30px 0 0;">
    <input type="hidden" name="id" value="${(imagesModel.id)!}"/>
    <div class="layui-form-item">
        <label class="layui-form-label">图片地址</label>
        <div class="layui-input-inline">
            <input type="text" class="layui-input" name="url" value="${(imagesModel.url)!}" autocomplete="off"
                   lay-verify="required" lay-reqtext="请填写正确的图片地址"/>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">1: 未删除 2：删除</label>
        <div class="layui-input-inline">
            <input type="text" class="layui-input" name="deletion" value="${(imagesModel.deletion)!}" autocomplete="off"
                   lay-verify="required" lay-reqtext="请填写正确的1: 未删除 2：删除"/>
        </div>
    </div>

    <div class="layui-form-item layui-hide">
        <input type="button" lay-submit lay-filter="LAY-back-submit" id="LAY-back-submit" value="确认">
    </div>
</div>
<script type="text/javascript" src="${ctx}/static/plugins/layuiadmin/layui/layui.js"></script>
<script type="text/javascript">
    layui.use(['form'], function () {
        var form = layui.form;
    });

</script>
</body>
</html>