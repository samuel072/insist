<#include "../common/common_ctx.ftl" />
<!DOCTYPE html>
<html	xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" type="text/css" href="${ctx}/static/plugins/layuiadmin/layui/css/layui.css"/>
<#--    <link rel="stylesheet" type="text/css" href="${ctx}/static/plugins/layuiadmin/style/admin.css"/>-->
<#--    <link rel="stylesheet" type="text/css" href="${ctx}/static/admin/css/base.css"/>-->
    <title>编辑用户详细信息</title>
</head>
<body>

    <div class="layui-form" lay-filter="form-submit"   style="padding: 20px 30px 0 0;">
        <input type="hidden" name="id" value="${(userModel.id)!}" />
        <div class="layui-form-item">
            <label class="layui-form-label">用户头像</label>
            <div class="layui-input-inline">
                <div class="layui-upload-drag" id="avatar">
                    <#if userModel?? && userModel.avatar?? && userModel.avatar != '' >
                        <img style="width: 100px;" src="${imageView}/${userModel.avatar}" class="layui-upload-img">
                    <#else>
                        <i class="layui-icon"></i>
                        <p>点击上传，或将文件拖拽到此处</p>
                    </#if>
                </div>
                <input type="hidden" name="avatar" value="${(userModel.avatar)!}" lay-verify="required" lay-reqtext="头像是必填项">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">用户名</label>
            <div class="layui-input-inline">
                <input type="text" class="layui-input" name="username" value="${(userModel.username)!}" autocomplete="off"
                       lay-verify="required|username" lay-reqtext="用户名是必须的" />
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">手机号</label>
            <div class="layui-input-inline">
                <input type="text" class="layui-input" name="phone" value="${(userModel.phone)!}" autocomplete="off"
                       lay-verify="required|phone" lay-reqtext="请填写正确的手机号" />
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">角色</label>
            <div class="layui-input-block">
                <#list roleList as role>
                <input type="radio" name="roleIds" value="${role.id}" title="${role.name}" lay-verify="required"
                       <#if codeList??>
                           <#list codeList as code>
                               <#if code == role.code>checked</#if>
                           </#list>
                       </#if>
                       lay-reqtext="请选择角色">
                </#list>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">状态</label>
            <div class="layui-input-inline">
                <select name="status" class="layui-select">
                    <option value="0" <#if userModel??&&userModel.status == 0>selected</#if>>启动</option>
                    <option value="1" <#if userModel??&&userModel.status == 1>selected</#if>>停用</option>
                </select>
            </div>
        </div>

        <div class="layui-form-item layui-hide">
            <input type="button" lay-submit lay-filter="LAY-back-submit" id="LAY-back-submit" value="确认">
        </div>
    </div>

    <script type="text/javascript" src="${ctx}/static/plugins/layuiadmin/layui/layui.js"></script>
    <script type="text/javascript">
        layui.use(['form', 'upload', 'element'], function() {
            var $ = layui.jquery;
            var upload = layui.upload;
            var element = layui.element;

            //执行实例
            var uploadInst = upload.render({
                elem: '#avatar', //绑定元素
                url: '${ctx}/upload/uploadThumb',
                method: "post",
                accept: 'images',
                acceptMime: 'image/*',
                progress: function(n){
                    var percent = n + '%';//获取进度百分比
                    element.progress('demo', percent); //可配合 layui 进度条元素使用
                },
                before: function(obj){ //obj参数包含的信息，跟 choose回调完全一致，可参见上文。
                    layer.load(); //上传loading
                    //预读本地文件示例，不支持ie8
                    obj.preview(function(index, file, result){
                        $('#avatar').html("");
                        $('#avatar').append('<img style="width: 100px;" src="'+ result +'" alt="'+ file.name +'" class="layui-upload-img">')
                    });
                },
                done: function(res){
                    //上传完毕回调
                    if (res.code = 10000) {
                        $("input[name='avatar']").val(res.message);
                    }
                    layer.closeAll('loading');
                }
                ,error: function(){
                    //请求异常回调
                    layer.closeAll('loading');
                }
            });
        });
    </script>
</body>
</html>