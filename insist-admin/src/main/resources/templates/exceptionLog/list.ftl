<#include "../common/common_ctx.ftl" />
<!DOCTYPE html>
<html	xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" type="text/css" href="${ctx}/static/plugins/layuiadmin/layui/css/layui.css"/>
    <link rel="stylesheet" type="text/css" href="${ctx}/static/plugins/layuiadmin/style/admin.css"/>
    <title>异常日志列表管理</title>
</head>
<body>
<div class="layui-fluid">
    <div class="layui-card">
        <div class="layui-form layui-card-header layuiadmin-card-header-auto">
            <div class="layui-form-item">
                <div class="layui-inline">
                    <label class="layui-form-label">日志名称</label>
                    <div class="layui-input-block">
                        <input class="layui-input" type="text" name="exceptionName" placeholder="请输入日志名称" autocomplete="off">
                    </div>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label">IP地址</label>
                    <div class="layui-input-block">
                        <input class="layui-input" type="text" name="ip" placeholder="请输入IP地址" autocomplete="off">
                    </div>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label">方法名称</label>
                    <div class="layui-input-block">
                        <input class="layui-input" type="text" name="method" placeholder="请输入方法名称" autocomplete="off">
                    </div>
                </div>
                <@shiro.hasPermission name="/exceptionLog/list">
                <div class="layui-inline">
                    <button class="layui-btn layuiadmin-btn-admin" lay-submit lay-filter="LAY-search">
                        <i class="layui-icon layui-icon-search layuiadmin-button-btn"></i>
                    </button>
                </div>
                </@shiro.hasPermission>
            </div>
        </div>

        <div class="layui-card-body">
            <table class="layui-table" lay-filter="exceptionLogTable" id="exceptionLogTable"></table>
        </div>
    </div>
</div>

<script type="text/html" id="tableHeadBar">
    <@shiro.hasPermission name="/exceptionLog/delete">
    <button type="button" class="layui-btn layuiadmin-btn-admin layui-btn-sm layui-btn-danger" lay-event="del">
        <i class="layui-icon">&#xe640;</i> 删除
    </button>
    </@shiro.hasPermission>
</script>
<script type="text/html" id="tableRightBar">
    <@shiro.hasPermission name="/exceptionLog/view">
    <a class="layui-btn layui-btn-xs" lay-event="view">查看</a>
    </@shiro.hasPermission>
</script>

<#include "../common/__ctx.ftl" />
<script type="text/javascript" src="${ctx}/static/admin/modular/exceptionLog.js"></script>

</body>
</html>