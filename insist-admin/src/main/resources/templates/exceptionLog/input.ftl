<#include "../common/common_ctx.ftl" />
<!DOCTYPE html>
<html	xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" type="text/css" href="${ctx}/static/plugins/layuiadmin/layui/css/layui.css"/>
    <title><#if exceptionLogModel?? &&
        exceptionLogModel.id??>编辑<#else>添加</#if>异常日志详细信息</title>
</head>
<body>

<div class="layui-form" lay-filter="form-submit" style="padding: 20px 30px 0 0;">
    <input type="hidden" name="id" value="${(exceptionLogModel.id)!}" />
        <div class="layui-form-item">
            <label class="layui-form-label">异常名称</label>
            <div class="layui-input-inline">
                字段信息：STRING
                <input type="text" class="layui-input" name="exceptionName" value="${(exceptionLogModel.exceptionName)!}" autocomplete="off"
                       lay-verify="required" lay-reqtext="请填写正确的异常名称" />
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">请求参数</label>
            <div class="layui-input-inline">
                字段信息：STRING
                <input type="text" class="layui-input" name="requestParams" value="${(exceptionLogModel.requestParams)!}" autocomplete="off"
                       lay-verify="required" lay-reqtext="请填写正确的请求参数" />
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">返回参数</label>
            <div class="layui-input-inline">
                字段信息：STRING
                <input type="text" class="layui-input" name="responseParams" value="${(exceptionLogModel.responseParams)!}" autocomplete="off"
                       lay-verify="required" lay-reqtext="请填写正确的返回参数" />
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">异常信息</label>
            <div class="layui-input-inline">
                字段信息：STRING
                <input type="text" class="layui-input" name="message" value="${(exceptionLogModel.message)!}" autocomplete="off"
                       lay-verify="required" lay-reqtext="请填写正确的异常信息" />
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">操作员ID</label>
            <div class="layui-input-inline">
                字段信息：STRING
                <input type="text" class="layui-input" name="userId" value="${(exceptionLogModel.userId)!}" autocomplete="off"
                       lay-verify="required" lay-reqtext="请填写正确的操作员ID" />
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">操作员名称</label>
            <div class="layui-input-inline">
                字段信息：STRING
                <input type="text" class="layui-input" name="userName" value="${(exceptionLogModel.userName)!}" autocomplete="off"
                       lay-verify="required" lay-reqtext="请填写正确的操作员名称" />
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">访问方法</label>
            <div class="layui-input-inline">
                字段信息：STRING
                <input type="text" class="layui-input" name="method" value="${(exceptionLogModel.method)!}" autocomplete="off"
                       lay-verify="required" lay-reqtext="请填写正确的访问方法" />
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">请求地址</label>
            <div class="layui-input-inline">
                字段信息：STRING
                <input type="text" class="layui-input" name="url" value="${(exceptionLogModel.url)!}" autocomplete="off"
                       lay-verify="required" lay-reqtext="请填写正确的请求地址" />
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">访问IP</label>
            <div class="layui-input-inline">
                字段信息：STRING
                <input type="text" class="layui-input" name="ip" value="${(exceptionLogModel.ip)!}" autocomplete="off"
                       lay-verify="required" lay-reqtext="请填写正确的访问IP" />
            </div>
        </div>

    <div class="layui-form-item layui-hide">
        <input type="button" lay-submit lay-filter="LAY-back-submit" id="LAY-back-submit" value="确认">
    </div>
</div>
<script type="text/javascript" src="${ctx}/static/plugins/layuiadmin/layui/layui.js"></script>
<script type="text/javascript">
    layui.use(['form'], function() {
        var form = layui.form;
    });

</script>
</body>
</html>