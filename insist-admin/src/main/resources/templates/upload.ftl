<#include "./common/common_ctx.ftl" />
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" type="text/css" href="${ctx}/static/plugins/layuiadmin/layui/css/layui.css"/>
    <link rel="shortcut icon" href="${ctx}/static/favicon.ico"/>
    <link rel="bookmark" href="${ctx}/static/favicon.ico"/>
    <title>登录</title>
</head>
<body style="background-color: rgb(242, 242, 242);">
<!-- 让IE8/9支持媒体查询，从而兼容栅格 -->
<!--[if lt IE 9]>
<script src="https://cdn.staticfile.org/html5shiv/r29/html5.min.js"></script>
<script src="https://cdn.staticfile.org/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<button type="button" class="layui-btn" id="test1">
    <i class="layui-icon">&#xe67c;</i>上传图片
</button>

<script type="text/javascript" src="${ctx}/static/jquery/jquery.js"></script>
<script type="text/javascript" src="${ctx}/static/plugins/layuiadmin/layui/layui.all.js"></script>
<script type="text/javascript">
$(function(){
    var upload = layui.upload;
    var layer = layui.layer;
    //执行实例
    var uploadInst = upload.render({
        elem: '#test1' //绑定元素
        ,url: '${ctx}/upload/uploadThumb' //上传接口
        ,accept: 'images'
        ,acceptMime: 'image/*'
        ,method: 'post'
        ,done: function(res){
            //上传完毕回调
            if (res.code == 10000) {
                console.info(res.message);
                layer.msg(res.message);
            }
            console.info(res);
            layer.msg(JSON.stringify(res));
        }
        ,error: function(){
            //请求异常回调
            layer.msg("异常回调啊");
        }
    });
});
</script>
</body>
</html>