<#include "./common/common_ctx.ftl" />
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="UTF-8">
	<meta name="renderer" content="webkit">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link rel="stylesheet" type="text/css" href="${ctx}/static/plugins/layuiadmin/layui/css/layui.css"/>
	<link rel="stylesheet" type="text/css" href="${ctx}/static/plugins/layuiadmin/style/admin.css"/>
	<link rel="stylesheet" type="text/css" href="${ctx}/static/admin/css/login.css"/>
	<link rel="shortcut icon" href="${ctx}/static/favicon.ico"/>
	<link rel="bookmark" href="${ctx}/static/favicon.ico"/>
<title>登录</title>
</head>
<body style="background-color: rgb(242, 242, 242);">
	<div class="login-layui-container layui-container">
		<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
	    	<legend>后台用户登录</legend>
	  	</fieldset>
	  	<form class="layui-form login-layui-form" action="${ctx}/toLogin" method="post">
			<div class="layui-form-item">
				<div class="layui-row">
					<div class="layui-col-xs11">
						<label class="layui-icon-input layui-icon layui-icon-username" for="LAY-phone"></label>
		    			<input type="text" name="phone" id="LAY-phone" required  lay-verify="required" placeholder="请输入手机号" autocomplete="off" class="layui-input">
					</div>
				</div>
		  	</div>
		  	<div class="layui-form-item">
		  		<div class="layui-row">
		  			<div class="layui-col-xs7">
		  				<label class="layui-icon-input layui-icon layui-icon-vercode" for="LAY-auth"></label>
		  				<input type="text" name="authCode" id="LAY-auth" required  lay-verify="required" placeholder="请输入验证码" autocomplete="off" class="layui-input" />
		  			</div>
		  			<div class="layui-col-xs5">
		  				<div style="margin-top:17px; margin-left:10px;">
		  					<button id="getCode" class="layui-btn layui-btn-primary">获取验证码</button>
		  				</div>
		  			</div>
		  		</div>
		  	</div>
			<div class="layui-form-item">
				<div class="layui-row">
					<input type="checkbox" name="rememberMe" lay-skin="switch" lay-text="记住我|请记住我"/>
				</div>
			</div>
		  	<div class="layui-form-item">
		    	<div class="layui-input-block">
		      		<button class="layui-btn layui-btn-normal" lay-submit lay-filter="loginForm">立即提交</button>
		      		<button type="reset" class="layui-btn layui-btn-primary">重置</button>
		    	</div>
		  	</div>
		</form>
   	</div>
   
<script type="text/javascript" src="${ctx}/static/jquery/jquery.js"></script>
<script type="text/javascript" src="${ctx}/static/plugins/layuiadmin/layui/layui.js"></script>
<script type="text/javascript">
	layui.use("layer", function(){
		var layer = layui.layer;
		var msg = "${(error)!}";
		if (msg !== "") {
			layer.msg(msg);
		}
	});
</script>
</body>
</html>