<#include "../common/common_ctx.ftl" />
<!DOCTYPE html>
<html	xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" type="text/css" href="${ctx}/static/plugins/layuiadmin/layui/css/layui.css"/>
    <link rel="stylesheet" type="text/css" href="${ctx}/static/admin/css/view.css"/>
    <title>查看任务管理详细信息</title>
</head>
<body>

<div class="layui-form" style="padding: 20px 30px 0 0;">
        <div class="layui-form-item">
            <label class="layui-form-label">任务名称</label>
            <div class="layui-input-block">
                    ${(taskCronModel.name)!}
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">任务编号</label>
            <div class="layui-input-block">
                    ${(taskCronModel.code)!}
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">任务执行规则</label>
            <div class="layui-input-block">
                    ${(taskCronModel.cron)!}
            </div>
        </div>
</div>
</body>
</html>