<#include "../common/common_ctx.ftl" />
<!DOCTYPE html>
<html	xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" type="text/css" href="${ctx}/static/plugins/layuiadmin/layui/css/layui.css"/>
    <link rel="stylesheet" type="text/css" href="${ctx}/static/admin/css/view.css"/>
    <title>查看操作日志详细信息</title>
</head>
<body>

<div class="layui-form" style="padding: 20px 30px 0 0;">
        <div class="layui-form-item">
            <label class="layui-form-label">操作模块</label>
            <div class="layui-input-block">
                    ${(operateLogModel.module)!}
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">操作类型</label>
            <div class="layui-input-block">
                    ${(operateLogModel.type)!}
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">操作描述</label>
            <div class="layui-input-block">
                    ${(operateLogModel.description)!}
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">请求参数</label>
            <div class="layui-input-block">
                    ${(operateLogModel.requestParams)!}
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">返回参数</label>
            <div class="layui-input-block">
                    ${(operateLogModel.responseParams)!}
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">用户名称</label>
            <div class="layui-input-block">
                    ${(operateLogModel.userName)!}
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">操作方法</label>
            <div class="layui-input-block">
                    ${(operateLogModel.method)!}
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">访问地址</label>
            <div class="layui-input-block">
                    ${(operateLogModel.url)!}
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">访问IP</label>
            <div class="layui-input-block">
                    ${(operateLogModel.ip)!}
            </div>
        </div>
</div>
</body>
</html>