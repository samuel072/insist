<#include "../common/common_ctx.ftl" />
<!DOCTYPE html>
<html	xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" type="text/css" href="${ctx}/static/plugins/layuiadmin/layui/css/layui.css"/>
    <title><#if operateLogModel?? &&
        operateLogModel.id??>编辑<#else>添加</#if>操作日志详细信息</title>
</head>
<body>

<div class="layui-form" lay-filter="form-submit" style="padding: 20px 30px 0 0;">
    <input type="hidden" name="id" value="${(operateLogModel.id)!}" />
        <div class="layui-form-item">
            <label class="layui-form-label">操作模块</label>
            <div class="layui-input-inline">
                    <input type="text" class="layui-input" name="module" value="${(operateLogModel.module)!}" autocomplete="off"
                           lay-verify="required" lay-reqtext="请填写正确的操作模块" />

            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">操作类型</label>
            <div class="layui-input-inline">
                    <input type="text" class="layui-input" name="type" value="${(operateLogModel.type)!}" autocomplete="off"
                           lay-verify="required" lay-reqtext="请填写正确的操作类型" />

            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">操作描述</label>
            <div class="layui-input-inline">
                    <input type="text" class="layui-input" name="description" value="${(operateLogModel.description)!}" autocomplete="off"
                           lay-verify="required" lay-reqtext="请填写正确的操作描述" />

            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">请求参数</label>
            <div class="layui-input-inline">
                    <input type="text" class="layui-input" name="requestParams" value="${(operateLogModel.requestParams)!}" autocomplete="off"
                           lay-verify="required" lay-reqtext="请填写正确的请求参数" />

            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">返回参数</label>
            <div class="layui-input-inline">
                    <input type="text" class="layui-input" name="responseParams" value="${(operateLogModel.responseParams)!}" autocomplete="off"
                           lay-verify="required" lay-reqtext="请填写正确的返回参数" />

            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">用户主键</label>
            <div class="layui-input-inline">
                    <input type="text" class="layui-input" name="userId" value="${(operateLogModel.userId)!}" autocomplete="off"
                           lay-verify="required" lay-reqtext="请填写正确的用户主键" />

            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">用户名称</label>
            <div class="layui-input-inline">
                    <input type="text" class="layui-input" name="userName" value="${(operateLogModel.userName)!}" autocomplete="off"
                           lay-verify="required" lay-reqtext="请填写正确的用户名称" />

            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">操作方法</label>
            <div class="layui-input-inline">
                    <input type="text" class="layui-input" name="method" value="${(operateLogModel.method)!}" autocomplete="off"
                           lay-verify="required" lay-reqtext="请填写正确的操作方法" />

            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">访问地址</label>
            <div class="layui-input-inline">
                    <input type="text" class="layui-input" name="url" value="${(operateLogModel.url)!}" autocomplete="off"
                           lay-verify="required" lay-reqtext="请填写正确的访问地址" />

            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">访问IP</label>
            <div class="layui-input-inline">
                    <input type="text" class="layui-input" name="ip" value="${(operateLogModel.ip)!}" autocomplete="off"
                           lay-verify="required" lay-reqtext="请填写正确的访问IP" />

            </div>
        </div>

    <div class="layui-form-item layui-hide">
        <input type="button" lay-submit lay-filter="LAY-back-submit" id="LAY-back-submit" value="确认">
    </div>
</div>
<script type="text/javascript" src="${ctx}/static/plugins/layuiadmin/layui/layui.js"></script>
<script type="text/javascript">
    layui.use(['form'], function() {
        var form = layui.form;
    });


</script>

</body>
</html>