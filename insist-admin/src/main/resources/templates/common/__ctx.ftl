<script type="text/javascript" src="${ctx}/static/jquery/jquery.js"></script>
<script type="text/javascript" src="${ctx}/static/plugins/layuiadmin/layui/layui.js"></script>
<script type="text/javascript">
    var Insist = {
        ctx: "",
        addCtx: function (ct) {
            if (this.ctx === "") {
                this.ctx = ct;
            }
        },
        msg: function (info) {
            top.layer.msg(info, {icon: 6});
        },
        success: function(info) {
            top.layer.msg(info, {icon: 1});
        },
        error: function (info) {
            top.layer.msg(info, {icon: 2});
        },
        confirm: function (tip, ensure) {
            top.layer.confirm(tip, {
                skin: 'layui-layer-admin'
            }, function () {
                ensure();
            });
        },
        commonFunction: function(table, url, tableId) {
            var pageIndex = layer.open({
                type: 2,
                content: Insist.ctx + url,
                maxmin: true,
                anim: 3,
                btnAlign: 'l',
                btn: ['确定', '取消'],
                yes: function (index, layero) {
                    // 获取到iframe层对象
                    var iframeWindow = window['layui-layer-iframe' + index];
                    // 获取iframe对象里面的点击保存按钮对象
                    var submitBtn = layero.find('iframe').contents().find('#LAY-back-submit');
                    //监听提交
                    iframeWindow.layui.form.on('submit(LAY-back-submit)', function (data) {
                        // 处理URL
                        var length = url.indexOf("?");
                        if (length != -1) {
                            url = url.substr(0, length);
                        }
                        var flag = Insist.operationFunc(url, data.field);
                        if (flag) {
                            table.reload(tableId); //数据刷新
                            layer.close(index); //关闭弹层
                        }

                    });
                    // 触发点击事件
                    submitBtn.trigger("click");
                }
            });
            layer.full(pageIndex);
        },
        operationFunc: function(url, data) {
            console.info("data: " + data);
            var flag = false;
            $.ajax({
                url: Insist.ctx + url,
                type: 'post',
                data: data,
                dataType: 'json',
                async: false,
                success: function (res) {
                    if (res.code === 10000) {
                        Insist.success(res.message);
                        flag = true;
                    } else {
                        Insist.error(res.message);
                    }
                }
            });
            return flag;
        },
        commonDel: function (table, url, tableId, data) {
            layer.confirm("您确定要删除吗？", function (index) {
                var ids = [];
                data.forEach(function (obj) {
                    ids.push(obj.id);
                });
                if (null == ids || ids.length <= 0) {
                    Insist.error("请至少选择一条记录！");
                    layer.close(index);
                    return false;
                }
                var $ids = ids.join(",");
                $.ajax({
                    url: Insist.ctx + url,
                    type: 'post',
                    data: {ids:$ids},
                    dataType: 'json',
                    async: false,
                    success: function (res) {
                        if (res.code === 10000) {
                            Insist.success(res.message);
                            table.reload(tableId);
                        } else {
                            Insist.error(res.message);
                        }
                    }
                });

                layer.close(index);
            }, function () {
                Insist.msg("你的选择是明智的！");
            });
        }

    };
    Insist.addCtx("${ctx}");
</script>