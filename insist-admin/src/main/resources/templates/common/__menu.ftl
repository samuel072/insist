<!DOCTYPE html>
<html	xmlns="http://www.w3.org/1999/xhtml" xmlns:shiro="http://www.pollix.at/thymeleaf/shiro">
<style type="text/css">
    .layui-side-menu .layui-nav .layui-nav-child .layui-nav-child a {
        padding-left: 45px;
    }
</style>
<div fragment="menu">
    <div class="layui-side layui-side-menu">
        <div class="layui-side-scroll">
            <div class="layui-logo" lay-href="/">
                <span>INSIST快速开发平台</span>
            </div>
            <ul class="layui-nav layui-nav-tree" lay-shrink="all" id="LAY-system-side-menu" lay-filter="layadmin-system-side-menu">
                <#list layuiTreeNodes as node>
                    <li data-name="set" class="layui-nav-item">
                        <a href="javascript:;" lay-tips="${node.title}" lay-direction="2">
                            <i class="layui-icon ${(node.icon)!'layui-icon-set'}"></i>
                            <cite>${node.title}</cite>
                        </a>
                        <@tree data=node/>

                    </li>
                </#list>
                <#-- freemarker 宏处理 -->
                <#macro tree data>
                    <#if data.children??>
                        <dl class="layui-nav-child">
                            <#list data.children as childrenNode>
                                <dd class="layui-nav-itemed" style="margin-left: 20px;">
                                    <#if childrenNode.children??>
                                        <a href="javascript:;">
                                            <i class="layui-icon ${(childrenNode.icon)!''}"></i>
                                            <cite>${childrenNode.title}</cite>
                                        </a>
                                        <@tree data=childrenNode />
                                    <#else>
                                        <a lay-href="${ctx}${childrenNode.href}">
                                            <i class="layui-icon ${(childrenNode.icon)!''}"></i>
                                            <cite>${childrenNode.title}</cite>
                                        </a>
                                    </#if>

                                </dd>
                            </#list>
                        </dl>
                    </#if>
                </#macro>
            </ul>
        </div>
    </div>


</div>

</html>