<#include "../common/common_ctx.ftl" />
<!DOCTYPE html>
<html	xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" type="text/css" href="${ctx}/static/plugins/layuiadmin/layui/css/layui.css"/>
    <link rel="stylesheet" type="text/css" href="${ctx}/static/plugins/layuiadmin/style/admin.css"/>
    <title>角色列表管理</title>
</head>
<body>
<div class="layui-fluid">
    <#-- 条件查询框 -->
    <div class="layui-card">
        <div class="layui-form layui-card-header layuiadmin-card-header-auto">
            <div class="layui-form-item">
                <div class="layui-inline">
                    <label class="layui-form-label">角色名</label>
                    <div class="layui-input-block">
                        <input class="layui-input" type="text" name="name" placeholder="请输入角色名" autocomplete="off">
                    </div>
                </div>

                <div class="layui-inline">
                    <label class="layui-form-label">角色代码</label>
                    <div class="layui-input-block">
                        <input class="layui-input" type="text" name="code" placeholder="请输入角色代码" autocomplete="off">
                    </div>
                </div>

                <#-- 搜索🔍框 -->
                <div class="layui-inline">
                    <button class="layui-btn layuiadmin-btn-admin" lay-submit lay-filter="LAY-search">
                        <i class="layui-icon layui-icon-search layuiadmin-button-btn"></i>
                    </button>
                </div>
            </div>
        </div>

        <#-- 表单 -->
        <div class="layui-card-body">
            <table class="layui-table" lay-filter="roleTable" id="roleTable"></table>
        </div>
    </div>
</div>

<#-- 工具栏 -->
<script type="text/html" id="tableHeadBar">
    <@shiro.hasPermission name="/role/save">
    <button type="button" class="layui-btn layuiadmin-btn-admin layui-btn-sm layui-btn-normal" lay-event="add">
        <i class="layui-icon">&#xe654;</i> 添加
    </button>
    </@shiro.hasPermission>
    <@shiro.hasPermission name="/role/delete">
    <button type="button" class="layui-btn layuiadmin-btn-admin layui-btn-sm layui-btn-danger" lay-event="del">
        <i class="layui-icon">&#xe640;</i> 删除
    </button>
    </@shiro.hasPermission>
</script>
<script type="text/html" id="tableRightBar">
    <@shiro.hasPermission name="/role/view">
    <a class="layui-btn layui-btn-xs" lay-event="view">查看</a>
    </@shiro.hasPermission>
    <@shiro.hasPermission name="/role/update">
    <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
    </@shiro.hasPermission>
    <@shiro.hasPermission name="/role/roleConfig">
    <a class="layui-btn layui-btn-xs" lay-event="roleConfig">权限配置</a>
    </@shiro.hasPermission>
</script>

<#include "../common/__ctx.ftl" />
<script type="text/javascript" src="${ctx}/static/admin/modular/role.js"></script>

</body>
</html>