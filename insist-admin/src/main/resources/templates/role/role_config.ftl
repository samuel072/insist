<#include "../common/common_ctx.ftl" />
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" type="text/css" href="${ctx}/static/plugins/layuiadmin/layui/css/layui.css"/>
    <link rel="stylesheet" type="text/css" href="${ctx}/static/plugins/layuiadmin/style/admin.css"/>
    <title>菜单列表</title>
</head>
<body>
<div class="layui-fluid">
    <div id="layui-tree" class="demo-tree"></div>
    <div class="layui-form">
        <input class="layui-hide" id="roleId" name="roleId" value="${(roleId)!}"/>
        <input class="layui-hide" id="purviewIds" name="purviewIds"/>
        <div class="layui-form-item layui-hide">
            <input type="button" lay-submit lay-filter="LAY-back-submit" id="LAY-back-submit" value="确认">
        </div>
    </div>
</div>

<#include "../common/__ctx.ftl" />
<script type="text/javascript">
    layui.use(["tree", "form"], function () {
        var tree = layui.tree;
        var form = layui.form;

        var treeObj = tree.render({
            elem: '#layui-tree',
            data: getTree(),
            isJump: true,
            id: 'menuTreeId',
            showCheckbox: true,
            oncheck: function(obj) {

            },
            click: function (obj) {
                $(".layui-tree-txt").css("background-color", "");
                $(".layui-tree-txt").css("color", "#555");
                obj.elem.find('.layui-tree-txt').first().css("background-color", "#c9c9c9");
                obj.elem.find('.layui-tree-txt').first().css("color", "#fff");
            }
        });

        function getTree() {
            var data = [];
            $.ajax({
                url: Insist.ctx + '/menu/allList?roleId=${roleId}',
                type: 'get',
                async: false,
                success: function (res) {
                    data = res;
                }
            });
            return data;
        }

        $("#LAY-back-submit").click(function () {
            var checkData = treeObj.getChecked();
            var $ids = jsonIds(checkData);
            if ($ids != undefined && $ids != null && $ids != '') {
                $("#purviewIds").val($ids);
            } else {
                Insist.error("请至少选择一项菜单！")
            }
        });

        function jsonIds(checkData) {
            var $ids;
            if (checkData.length > 0) {
                var ids = [];
                checkData.forEach(function (obj) {
                    ids.push(obj.id);
                    var children = obj.children;
                    if (children != undefined && children != null && children.length > 0) {
                        var childrenId = jsonIds(children);
                        ids.push(childrenId);
                    }
                });
                $ids = ids.join(",");

            }
            return $ids;
        }
    });
</script>
</body>
</html>