<#include "../common/common_ctx.ftl" />
<!DOCTYPE html>
<html	xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" type="text/css" href="${ctx}/static/plugins/layuiadmin/layui/css/layui.css"/>
    <title><#if loginLogModel?? &&
        loginLogModel.id??>编辑<#else>添加</#if>登录日志详细信息</title>
</head>
<body>

<div class="layui-form" lay-filter="form-submit" style="padding: 20px 30px 0 0;">
    <input type="hidden" name="id" value="${(loginLogModel.id)!}" />
        <div class="layui-form-item">
            <label class="layui-form-label">登录用户</label>
            <div class="layui-input-inline">
                <input type="text" class="layui-input" name="loginName" value="${(loginLogModel.loginName)!}" autocomplete="off"
                       lay-verify="required" lay-reqtext="请填写正确的登录用户" />
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">日志名称</label>
            <div class="layui-input-inline">
                <input type="text" class="layui-input" name="logName" value="${(loginLogModel.logName)!}" autocomplete="off"
                       lay-verify="required" lay-reqtext="请填写正确的日志名称" />
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">ip地址</label>
            <div class="layui-input-inline">
                <input type="text" class="layui-input" name="ip" value="${(loginLogModel.ip)!}" autocomplete="off"
                       lay-verify="required" lay-reqtext="请填写正确的ip地址" />
            </div>
        </div>

    <div class="layui-form-item layui-hide">
        <input type="button" lay-submit lay-filter="LAY-back-submit" id="LAY-back-submit" value="确认">
    </div>
</div>
<script type="text/javascript" src="${ctx}/static/plugins/layuiadmin/layui/layui.js"></script>
<script type="text/javascript">
    layui.use(['form'], function() {
        var form = layui.form;
    });

</script>
</body>
</html>