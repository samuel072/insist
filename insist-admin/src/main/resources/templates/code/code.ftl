<#include "../common/common_ctx.ftl" />
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:th="http://www.thymeleaf.org">
<head>
    <meta charset="UTF-8">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" type="text/css" href="${ctx}/static/plugins/layuiadmin/layui/css/layui.css" />
    <link rel="stylesheet" type="text/css" href="${ctx}/static/plugins/layuiadmin/style/admin.css" />
    <link rel="stylesheet" type="text/css" href="${ctx}/static/admin/css/base.css"/>
    <link rel="shortcut icon" href="${ctx}/static/favicon.ico"/>
    <link rel="bookmark" href="${ctx}/static/favicon.ico"/>
    <title>代码生成</title>
</head>
<body>
    <div class="layui-fluid">
        <div class="layui-card">

            <div class="layui-card-body">
                <form class="layui-form" style="padding: 20px 30px 0 0;">
                    <div class="layui-form-item">
                        <label class="layui-form-label">作者 <i class="layui-icon layui-icon-tips" lay-tips="代码生成的注释人"></i></label>
                        <div class="layui-input-inline">
                            <input type="text" name="authorName" lay-verify="required" lay-reqText="请输入作者名称" placeholder="代码生成的注释人" autocomplete="off" class="layui-input">
                        </div>
                        <div class="layui-input-inline">
                            <input type="checkbox" name="indexPageSwitch" title="生成主页面" lay-skin="primary" value="true" checked="">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">业务名称 <i class="layui-icon layui-icon-tips" lay-tips="输入中文业务名称"></i></label>
                        <div class="layui-input-inline">
                            <input type="text" name="bizChName" lay-verify="required" lay-reqText="输入中文业务名称"
                                   class="layui-input" placeholder="输入中文业务名称">
                        </div>
                        <div class="layui-input-inline">
                            <input type="checkbox" name="addPageSwitch" title="生成添加页面" lay-skin="primary" value="true" checked="">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">项目包名 <i class="layui-icon layui-icon-tips" lay-tips="项目的包名"></i></label>
                        <div class="layui-input-inline">
                            <input type="text" name="parentPackagePath" lay-verify="required" lay-reqText="请输入项目的包" placeholder="项目的包名 eg: com.pan.insist" autocomplete="off" class="layui-input">
                        </div>
                        <div class="layui-input-inline">
                            <input type="checkbox" name="editPageSwitch" title="生成编辑页面" lay-skin="primary" value="true" checked="">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">表名称 <i class="layui-icon layui-icon-tips" lay-tips="表名称"></i></label>
                        <div class="layui-input-inline">
                            <input type="text" name="tableName" lay-verify="required" placeholder="请输入表名称" lay-reqText="请输入表名称" autocomplete="off" class="layui-input">
                        </div>
                        <div class="layui-input-inline">
                            <input type="checkbox" name="infoJsSwitch" title="生成详情页" lay-skin="primary" value="true" checked="">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">表前缀 <i class="layui-icon layui-icon-tips" lay-tips="影响实体类名称"></i></label>
                        <div class="layui-input-inline">
                            <input type="text" name="tablePrefix" placeholder="请输入表前缀" autocomplete="off" class="layui-input">
                        </div>
                        <div class="layui-input-inline">
                            <input type="checkbox" name="jsSwitch" title="生成JS" lay-skin="primary" value="true" checked="">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <div class="layui-input-block">
                            <button class="layui-btn layui-btn-danger" lay-submit lay-filter="LAY-user-front-submit">确认</button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>

    <#include "../common/__ctx.ftl" />
    <script type="text/javascript" src="${ctx}/static/jquery/jquery.js"></script>
    <script type="text/javascript" src="${ctx}/static/plugins/layuiadmin/layui/layui.js"></script>
    <script>
        layui.config({
            base: '${ctx}/static/plugins/layuiadmin/' //静态资源所在路径
        }).extend({
            index: 'lib/index' //主入口模块
        }).use(['index', 'form'], function(){
            var form = layui.form ;
            //监听提交
            form.on('submit(LAY-user-front-submit)', function(data){
                $.ajax({
                    url:"${ctx}/code/execute",
                    async: false,
                    type:"POST",
                    dataType: "json",
                    data: data.field,
                    success: function(data){
                        if (data.code = 10000) {
                            Insist.success(data.message);
                        } else {
                            Insist.error(data.message);
                        }
                    }
                });
                return false;
            });
        });
    </script>
</body>
</html>