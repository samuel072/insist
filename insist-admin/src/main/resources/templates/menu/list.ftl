<#include "../common/common_ctx.ftl" />
<!DOCTYPE html>
<html	xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" type="text/css" href="${ctx}/static/plugins/layuiadmin/layui/css/layui.css"/>
    <link rel="stylesheet" type="text/css" href="${ctx}/static/plugins/layuiadmin/style/admin.css"/>
    <title>菜单列表管理</title>
</head>
<body>
<div class="layui-fluid">
    <#-- 条件查询框 -->
    <div class="layui-card">
        <div class="layui-form layui-card-header layuiadmin-card-header-auto">
            <div class="layui-form-item">
                <div class="layui-inline">
                    <label class="layui-form-label">菜单名</label>
                    <div class="layui-input-block">
                        <input class="layui-input" type="text" name="name" placeholder="请输入菜单名" autocomplete="off">
                    </div>
                </div>

                <div class="layui-inline">
                    <label class="layui-form-label">菜单地址</label>
                    <div class="layui-input-block">
                        <input class="layui-input" type="text" name="url" placeholder="请输入菜单地址" autocomplete="off">
                    </div>
                </div>

                <#-- 搜索🔍框 -->
                <div class="layui-inline">
                    <button class="layui-btn layuiadmin-btn-admin" lay-submit lay-filter="LAY-search">
                        <i class="layui-icon layui-icon-search layuiadmin-button-btn"></i>
                    </button>
                </div>
            </div>
        </div>

        <#-- 表单 -->
        <div class="layui-card-body">
            <table class="layui-table" lay-filter="menuTable" id="menuTable"></table>
        </div>
    </div>
</div>

<#-- 工具栏 -->
<script type="text/html" id="tableHeadBar">
    <@shiro.hasRole name="admin">
    <button type="button" class="layui-btn layuiadmin-btn-admin layui-btn-sm layui-btn-normal" lay-event="add">
        <i class="layui-icon">&#xe654;</i> 添加
    </button>
    <button type="button" class="layui-btn layuiadmin-btn-admin layui-btn-sm layui-btn-danger" lay-event="del">
        <i class="layui-icon">&#xe640;</i> 删除
    </button>
    </@shiro.hasRole>
</script>
<script type="text/html" id="tableRightBar">
    <@shiro.hasRole name="admin">
    <a class="layui-btn layui-btn-xs" lay-event="view">查看</a>
    <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
    </@shiro.hasRole>
</script>

<#include "../common/__ctx.ftl" />
<script type="text/javascript" src="${ctx}/static/admin/modular/menu.js"></script>

</body>
</html>