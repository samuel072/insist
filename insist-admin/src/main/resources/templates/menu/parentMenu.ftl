<#include "../common/common_ctx.ftl" />
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" type="text/css" href="${ctx}/static/plugins/layuiadmin/layui/css/layui.css"/>
    <link rel="stylesheet" type="text/css" href="${ctx}/static/plugins/layuiadmin/style/admin.css"/>
    <title>父菜单列表</title>
</head>
<body>
<div class="layui-fluid">
    <div id="layui-tree" class="demo-tree"></div>
    <div class="layui-form">
        <input class="layui-hide" id="treeId" name="treeId"/>
        <input class="layui-hide" id="treeName" name="treeName"/>
        <div class="layui-form-item layui-hide">
            <input type="button" lay-submit lay-filter="LAY-back-submit" id="LAY-back-submit" value="确认">
        </div>
    </div>
</div>

<#include "../common/__ctx.ftl" />
<script type="text/javascript" src="${ctx}/static/admin/modular/common-tree.js"></script>
</body>
</html>