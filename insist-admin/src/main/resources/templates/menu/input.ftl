<#include "../common/common_ctx.ftl" />
<!DOCTYPE html>
<html	xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="stylesheet" type="text/css" href="${ctx}/static/plugins/layuiadmin/layui/css/layui.css"/>
    <title>编辑角色详细信息</title>
</head>
<body>

<div class="layui-form" lay-filter="form-submit"   style="padding: 20px 30px 0 0;">
    <input type="hidden" name="id" value="${(menuModel.id)!}" />
    <div class="layui-form-item">
        <label class="layui-form-label">上级菜单<span style="color: red;">*</span></label>
        <div class="layui-input-inline">
            <input type="hidden" id="pid" name="pid" value="${(menuModel.pid)!0}" lay-verify="required">
            <input type="text" class="layui-input" id="pidName" name="pidName" autocomplete="off" lay-verify="required"
                   placeholder="请选择上级菜单" value="${(menuModel.pidName)!'顶级父类'}">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">菜单名<span style="color: red;">*</span></label>
        <div class="layui-input-inline">
            <input type="text" class="layui-input" name="name" value="${(menuModel.name)!}" autocomplete="off"
                   lay-verify="required|username" lay-reqtext="请填写正确的菜单名" />
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">菜单地址<span style="color: red;">*</span></label>
        <div class="layui-input-inline">
            <input type="text" class="layui-input" name="url" value="${(menuModel.url)!}" autocomplete="off"
                   lay-verify="required" lay-reqtext="请填写菜单地址" />
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">是否是菜单<span style="color: red;">*</span></label>
        <div class="layui-input-inline">
            <select name="menu" lay-verify="required" lay-reqtext="请选择是否是菜单项">
                <#if menuModel?? && menuModel.menu??>
                <option value="1" <#if menuModel.menu == 1>selected</#if>>菜单</option>
                <option value="2" <#if menuModel.menu != 1>selected</#if>>按钮</option>
                <#else>
                <option value="1">菜单</option>
                <option value="2" selected>按钮</option>
                </#if>
            </select>
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">图标</label>
        <div class="layui-input-inline">
            <input type="text" class="layui-input" name="icon" value="${(menuModel.icon)!}" placeholder="layui官网去找" autocomplete="off" />
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">顺序</label>
        <div class="layui-input-inline">
            <input type="number" class="layui-input" name="orderNum" value="${(menuModel.orderNum)!9999}" placeholder="默认9999" autocomplete="off" />
        </div>
    </div>

    <div class="layui-form-item layui-hide">
        <input type="button" lay-submit lay-filter="LAY-back-submit" id="LAY-back-submit" value="确认">
    </div>
</div>

<script type="text/javascript">
    var Insist = {
        ctx: "",
        addCtx: function (ct) {
            if (this.ctx === "") {
                this.ctx = ct;
            }
        },
        info: function (info) {
            top.layer.msg(info, {icon: 6});
        },
        success: function(info) {
            top.layer.msg(info, {icon: 1});
        },
        error: function (info) {
            top.layer.msg(info, {icon: 2});
        }
    };
    Insist.addCtx("${ctx}");
</script>
<script type="text/javascript" src="${ctx}/static/plugins/layuiadmin/layui/layui.js"></script>
<script type="text/javascript">
    layui.use('form', function(){
        var form = layui.form;
        var $ = layui.$;
        $("#pidName").click(function () {
            layer.open({
                type: 2,
                title: '父级菜单',
                content: Insist.ctx + '/menu/parentMenu',
                btn: ['确定', '返回'],
                yes: function (index, layero) {
                    // 获取到iframe层对象
                    var iframeWindow = window['layui-layer-iframe' + index];
                    // 获取iframe对象里面的点击保存按钮对象
                    var submitBtn = layero.find('iframe').contents().find('#LAY-back-submit');
                    //监听提交
                    iframeWindow.layui.form.on('submit(LAY-back-submit)', function (data) {
                        const $pid = data.field.treeId;
                        if ($pid == null || $pid == '' || $pid == undefined) {
                            $("#pid").val(0);
                            $("#pidName").val("顶级父类");
                        } else {
                            $("#pid").val($pid);
                            $("#pidName").val(data.field.treeName);
                        }
                        //关闭弹层
                        layer.close(index);
                    });
                    // 触发点击事件
                    submitBtn.trigger("click");
                }
            });
        });
    });
</script>
</body>
</html>