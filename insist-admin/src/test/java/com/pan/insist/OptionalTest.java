package com.pan.insist;



import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Test;

/**
 * @author aneng huang
 * @since 2020-07-17 11:11:48
 */
public class OptionalTest {


	@Test
    public void listNull() {
        List<String> list = null;

        Optional.ofNullable(list).orElse(new ArrayList<>()).stream().forEach(System.out::println);
    }
}
