package com.pan.insist;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.alibaba.fastjson.JSONObject;
import com.pan.insist.bean.BaseResponse;
import com.pan.insist.constant.CommonConstant;
import com.pan.insist.service.WeChatService;

@SpringBootTest
@WebAppConfiguration
@RunWith(SpringRunner.class)
public class WeChatServiceImplTest {


    @Autowired
    private WeChatService weChatService;

    @Test
    public void getAccessTokenTest() {
        BaseResponse accessToken = weChatService.getAccessToken();
        System.out.println(JSONObject.toJSON(accessToken).toString());
    }


    @Test
    public void qrCodeTest() {
        BaseResponse accessToken = weChatService.getAccessToken();
        if (CommonConstant.Code.FAIL_CODE == accessToken.getCode()) {
            System.out.println("获取accessToken失败！原因：" + accessToken.getMessage());
            return;
        }

        BaseResponse baseResponse = weChatService.loginQrCode(accessToken.getParams());
        System.out.println("获取临时二维码结果：" + JSONObject.toJSON(baseResponse).toString());

        baseResponse = weChatService.loginLimitQrCode(accessToken.getParams());
        System.out.println("获取永久二维码结果：" + JSONObject.toJSON(baseResponse).toString());
    }

}
