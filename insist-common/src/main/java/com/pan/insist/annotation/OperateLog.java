package com.pan.insist.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>
 *    日志操作注解
 * </p>
 *
 * @author kaiji
 * @since 2020/1/20 09:59
 */
@Inherited
@Documented
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface OperateLog {

    // 操作模块
    String operateModule() default "";
    // 操作类型
    String operateType() default "";
    // 操作说明
    String operateDesc() default "";
}
