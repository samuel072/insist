package com.pan.insist.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.util.concurrent.TimeUnit;

/**
 * <p>避免重复提交</p>
 * 
 * @author kaiji
 * @since  20190929
 */

@Documented
@Inherited
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface CacheLock {

	/**
	 *	redis 锁的前缀 
	 */
	String prefix() default "";
	
	/**
	 *	默认过期时间
	 *
	 * @return 默认5秒
	 */
	int expire() default 5;
	
	/**
	 * 过期时间单位
	 * 
	 * @return 单位秒
	 */
	TimeUnit timeUnit() default TimeUnit.SECONDS;
	
	/**
	 * Key的分隔符（默认 :）
	 * 
	 * @return String
	 */
	String delimiter() default ":";
}
