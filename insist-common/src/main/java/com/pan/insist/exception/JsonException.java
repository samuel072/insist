package com.pan.insist.exception;

/**
 * <p>
 *
 * @author kaiji
 * @description 返回JSON格式的异常
 * </p>
 * @since 2020/1/16 11:22
 */
public class JsonException extends RuntimeException {

    private static final long serialVersionUID = -7783669020442618555L;

    //错误码
    private Integer code;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public JsonException(String message) {
        super(message);
    }

    /**
     * 构造器重载，主要是自己考虑某些异常自定义一些返回码
     *
     * @param code    错误返回码
     * @param message 错误返回信息
     */
    public JsonException(Integer code, String message) {
        super(message);
        this.code = code;
    }
}
