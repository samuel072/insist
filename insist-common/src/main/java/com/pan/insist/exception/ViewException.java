package com.pan.insist.exception;

/**
 * <p>
 *
 * @author kaiji
 * @description 返回页面的错误异常
 * </p>
 * @date 2020-01-16
 */
public class ViewException extends RuntimeException {

    private static final long serialVersionUID = 6939520975388661433L;

    /** 错误码 **/
    private Integer code;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public ViewException(String message) {
        super(message);
    }

    /**
     * 构造器重载，主要是自己考虑某些异常自定义一些返回码
     *
     * @param code    错误返回码
     * @param message 错误返回信息
     */
    public ViewException(Integer code, String message) {
        super(message);
        this.code = code;
    }
}
