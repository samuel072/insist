package com.pan.insist.bean;

import lombok.Data;

/**
 * 返回值
 * @author kaiji
 */
@Data
public class BaseResponse {

    private int code;
    private String message;
    private String params;

    public BaseResponse(int code, String message) {
        this.code = code;
        this.message = message;
    }
    public BaseResponse(int code, String message, String params) {
        this.code = code;
        this.message = message;
        this.params = params;
    }

}
