package com.pan.insist.util;

import java.util.UUID;

/**
 * <p>
 *  生成UUID
 * </p>
 *
 * @author kaiji
 * @since 2020/1/10 14:25
 */
public class UuidUtil {

    public static final String getUuid() {
        return UUID.randomUUID().toString().replaceAll("-","");
    }
}
