package com.pan.insist.util;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.MonthDay;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

/**
 * <p>
 *     localDateTime操作工具类
 * </p>
 *
 * @author kaiji
 * @since 2020/1/19 20:09
 */
public class DateUtil {

    private static final String DEFAULT_FORMAT = "yyyy-MM-dd HH:mm:ss";
    private static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd";
    private static final String DEFAULT_TIME_FORMAT = "HH:mm:ss";
    
    static DateTimeFormatter DATE_TIME_DEFAULT_FORMAT = DateTimeFormatter.ofPattern(DEFAULT_FORMAT);
    static DateTimeFormatter DATE_DEFAULT_FORMAT = DateTimeFormatter.ofPattern(DEFAULT_DATE_FORMAT);
    static DateTimeFormatter TIME_DEFAULT_FORMAT = DateTimeFormatter.ofPattern(DEFAULT_TIME_FORMAT);
    
    /**
     * <p>
     *	时间转字符串
     * </p>
     * @param time
     * 	 年、月、日 时：分：秒
     * @return 
     * @return str：yyyy-MM-dd HH:mm:ss
     */
    public static String dateTime2Str(LocalDateTime time) {
    	return Optional.of(time)
    			.map(u -> u.format(DATE_TIME_DEFAULT_FORMAT)).get();
    }
    
    /**
     * <p>
     *	时间转字符串
     * </p>
     * @param time
     * 	 年、月、日 时：分：秒
     * @return 
     * @return str：yyyy-MM-dd
     */
    public static String date2Str(LocalDateTime time) {
    	return Optional.of(time)
    			.map(u -> u.format(DATE_DEFAULT_FORMAT)).get();
    }
    
    /**
     * <p>
     *	时间转字符串
     * </p>
     * @param time
     * 	 年、月、日 时：分：秒
     * @return 
     * @return str：HH:mm:ss
     */
    public static String time2Str(LocalDateTime time) {
    	return Optional.of(time)
    			.map(u -> u.format(TIME_DEFAULT_FORMAT)).get();
    }
    
    public static LocalDateTime str2DateTime(String str) {
        return Optional.of(str)
    			.map(u -> LocalDateTime.parse(str, DATE_TIME_DEFAULT_FORMAT)).get();
	}
    
    /**
     * <p>
     *	比对两个日期是否相等
     * </p>
     * @param date1
     * 		时间1
     * @param date2
     * 		时间2
     * @return true：相等 false：不相等
     */
    public static boolean dateEqual(LocalDate date1, LocalDate date2) {
         if(date1.equals(date2)){
             return true;
         }else{
        	 return false;
         }
    }
    
    /**
     * <p>
     *	比对两个时间的月份和天是否相等
     * </p>
     * @param date1
     * @param date2
     * @return
     */
    public static boolean monthDayEqual(LocalDate date1, LocalDate date2) {
         MonthDay birthday = MonthDay.of(date2.getMonth(), date2.getDayOfMonth());
         MonthDay currentMonthDay = MonthDay.from(date1);

         if(currentMonthDay.equals(birthday)){
        	 return true;
         }else{
        	 return false;
         }
	}
    
    /**
     * <p>
     *	date时间的后num个小时
     * </p>
     * @param date
     * 		date：11:00:57 时、分、秒
     * @param num
     * 		date后的num时间值
     * @return 值的num后的时间
     */
    public static LocalTime plusTime(LocalTime date, int num) {
        return date.plusHours(num);
	}
    
    /**
     * <p>
     *	date时间的前num个小时
     * </p>
     * @param date
     * 		date：11:00:57 时、分、秒
     * @param num
     * 		date前的num时间值
     * @return 值的num前的时间
     */
    public static LocalTime minusHours(LocalTime date, int num) {
        return date.minusHours(num);
	}
    
    /**
     * <p>
     *	计算date后的num星期
     * </p>
     * @param date
     * 	年、月、日 2020-03-13
     * @param num
     * 	推算num后的值
     * @return  um后的星期值
     */
    public static LocalDate plusWeeks(LocalDate date, int num) {
    	return date.plus(1, ChronoUnit.WEEKS);
	}
    
    /**
     * <p>
     *	获取当前时间的时间戳
     * </p>
     * @return	当前时间的时间戳
     */
    public static long nowTimestamp() {
    	Instant timestamp = Instant.now();
		return timestamp.toEpochMilli();
	}
    
    public static void name() {
    	LocalDate today = LocalDate.now();
        LocalDate java8Release = LocalDate.of(2018, 12, 14);
        Period periodToNextJavaRelease = Period.between(today, java8Release);
        System.out.println("Months left between today and Java 8 release : "
                + periodToNextJavaRelease.getMonths() );
    }
    
    public static void main(String[] args) {
    	LocalDate today = LocalDate.now();
    	// 2020-03-13
        LocalDate java8Release = LocalDate.of(2017, 12, 14);

        Period periodToNextJavaRelease = Period.between(today, java8Release);
        System.out.println("Months left between today and Java 8 release : "
                + periodToNextJavaRelease.getMonths() );
        System.out.println("Months left between today and Java 8 release : "
                + periodToNextJavaRelease.getDays() );
        System.out.println("Months left between today and Java 8 release : "
                + periodToNextJavaRelease.getYears() );
	}
}
