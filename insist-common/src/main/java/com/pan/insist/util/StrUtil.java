package com.pan.insist.util;

import java.util.Optional;
import java.util.regex.Matcher;

/**
 * <p>
 *  字符串的一些工具
 * </p>
 *
 * @author kaiji
 * @since 2020/1/16 09:10
 */
public class StrUtil {

    /**
     * 下划线转驼峰
     *
     * @param str
     *      字符串
     * @return  一个驼峰命名之后的字符串
     */
    public static String lineToHump(String str) {
        Matcher matcher = RegexUtil.linePattern(str);
        StringBuffer sb = new StringBuffer();
        while (matcher.find()) {
            matcher.appendReplacement(sb, matcher.group(1).toUpperCase());
        }
        matcher.appendTail(sb);
        return sb.toString();
    }

    /**
     * 将字符串中的连续的多个换行缩减成一个换行
     * @param str  要处理的内容
     * @return	返回的结果
     */
    public static String replaceLineBlanks(String str) {
        String result = null;
        if (str != null) {
            result = str.replaceAll("\\n", "");
        }
        return Optional.ofNullable(result).orElse(str);
    }

    public static void main(String[] args) {
        String str = "  FROM t_menu WHERE (menu = ?):1:MybatisSqlSessionFactoryBean";

        String s = replaceLineBlanks(str);
        System.out.println(s);

    }
}
