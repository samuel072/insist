package com.pan.insist.util;

import org.apache.commons.lang3.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 正则表达式枚举
 *
 * @author kaiji
 * @since 2019-10-12
 */
public class RegexUtil {


    private static final String REGEX_PHONE = "^((13[0-9])|(14[579])|(15([0-3,5-9]))|(16[6])|(17[0135678])|(18[0-9]|19[89]))\\d{8}$";
    private static final String REGEX_USERNAME = "^(?!_)(?!.*?_$)[a-zA-Z0-9_\u4e00-\u9fa5]+$";
    private static final String REGEX_PASSWORD = "^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{6,16}$";
    private static final String LINE_PATTERN = "_([a-z])";
    private static final String NEW_LINE_CHAR = "(\r?\n(\\s*\r?\n)+)";

    /**
     * 判断手机号是否符合规则
     *
     * @param phone
     *      手机号
     * @return
     *      true：是 false: 否
     */
    public static boolean matchMobile(String phone) {
        if (StringUtils.isEmpty(phone)) {
            return false;
        }
        Pattern p = Pattern.compile(REGEX_PHONE);
        Matcher m = p.matcher(phone);
        return m.matches();

    }

    /**
     * 判断用户名称是否符合规则
     *
     * @param username
     *      用户名称
     * @return true：符合 false：不符合
     */
    public static boolean matchUsername(String username) {
        if (StringUtils.isBlank(username)) {
            return false;
        }
        Pattern p = Pattern.compile(REGEX_USERNAME);
        Matcher m = p.matcher(username);
        return m.matches();
    }
    
    /**
     * 判断密码是否符合规则
     *
     * @param password
     *      密码
     * @return true：符合 false：不符合
     */
    public static boolean matchPassword(String password) {
    	if (StringUtils.isBlank(password)) {
    		return false;
    	}
    	Pattern p = Pattern.compile(REGEX_PASSWORD);
    	Matcher m = p.matcher(password);
    	return m.matches();
    }

    public static Matcher linePattern(String str) {
        Pattern p = Pattern.compile(LINE_PATTERN);
        return p.matcher(str);
    }

    /**
     * 匹配换行符
     *
     * @param str
     *      配置的字符串
     * @return  true： 配置成功 false: 失败
     */
    public static Matcher newLineChar(String str) {
        Pattern p = Pattern.compile(NEW_LINE_CHAR);
        return p.matcher(str);
    }



    public static void main(String[] args) {
        boolean username = matchUsername("黄阿能_1");
        System.out.println(username);
    }

}
