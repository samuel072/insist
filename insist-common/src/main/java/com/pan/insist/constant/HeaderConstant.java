package com.pan.insist.constant;

/**
 * <p>
 *  请求信息的头部信息
 * </p>
 *
 * @author kaiji
 * @since 2020/1/17 10:30
 */
public class HeaderConstant {

    /** Referer **/
    public static final String REFERER = "Referer";
}
