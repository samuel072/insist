package com.pan.insist.constant;

/**
 * <p>
 *  操作枚举
 * </p>
 *
 * @author kaiji
 * @since 2020/1/20 09:51
 */
public class OperateConstant {

    public static final String SAVE = "save";
    public static final String DELETE = "delete";
    public static final String UPDATE = "update";
    public static final String SELECT = "select";
    public static final String VIEW = "view";

}
