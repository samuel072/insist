package com.pan.insist.constant;


/**
 * Bean类 - 公共常量
 * @author kaiji
 */
public class CommonConstant {

	/** 点号 **/
	public static final String SPOT = ".";
	/** 逗号 **/
	public static final String DOC_FLAG = ",";
	/** 冒号 **/
	public static final String COLON_FLAG = ":";
	/** 星号 * **/
	public static final String ASTERISK = "*";
	/** 默认一些数值 **/
	public static final int MAGIC_VALUE_ZERO = 0;
	public static final int MAGIC_VALUE_ONE = 1;
	public static final int MAGIC_VALUE_TWO = 2;
	public static final int MAGIC_VALUE_THREE = 3;
	public static final int MAGIC_VALUE_FOUR = 4;
	public static final int MAGIC_VALUE_FIVE = 5;
	public static final int MAGIC_VALUE_SIXTEEN= 16;
	public static final int FIVE_HUNDRED = 500;

	public static final String MAGIC_VALUE_STR_ZERO = "0";
	public static final String MAGIC_VALUE_STR_ONE = "1";
	public static final String MAGIC_VALUE_STR_TWO = "2";
	public static final String MAGIC_VALUE_STR_THREE = "3";
	public static final String MAGIC_VALUE_STR_FOUR = "4";

	/** 修改数据类型 **/
	public static final int DATA_UPDATE_TYPE = MAGIC_VALUE_ONE;
	/** 存储数据类型 **/
	public static final int DATA_SAVE_TYPE = MAGIC_VALUE_TWO;
	/** 默认密码 **/
	public static final String PASSWORD = "123456";
	/** map初始化大小 **/
	public static final int DEFAULT_INITIAL_CAPACITY = MAGIC_VALUE_SIXTEEN;
	/** 超级管理员 **/
	public static final String ADMIN = "admin";



    /** 环境类型 */
	public static final class Environment {
		public static final String LOCAL = "local";
		public static final String ONLINE = "online";
	}

	/** ajax返回码 */
	public static final class AjaxCode {
		// 返回消息key
		public static final String CODE = "code";
		public static final Integer SUCCESS_CODE = 10000;
		public static final Integer FAIL_CODE = 90001;
		// 消息key
		public static final String MESSAGE = "message";
	}

	public static final class ExceptionClazz {
		/** 错误的返回码 **/
		public static final int RESPONSE_ERROR_CODE = FIVE_HUNDRED;
	}

	/**
	 * 微信中需要用到的魔法值
	 */
	public static final class WeChat {
		/** 微信获取accessToken的Redis的key **/
		public static final String ACCESS_TOKEN_REDIS_KEY = "access_token_redis_value";
		public static final String ERROR_CODE = "errcode";
		public static final String ACCESS_TOKEN = "access_token";
		public static final String EXPIRES_IN = "expires_in";
		public static final String ERROR_MSG = "errmsg";
	}

	/**
	 * 整个项目的返回码
	 */
	public static final class Code {
		public static final Integer SUCCESS_CODE = 10000;
		public static final Integer FAIL_CODE = 90001;
	}


}
