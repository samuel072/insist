package com.pan.insist.service;

import com.pan.insist.bean.BaseResponse;

/**
 * 微信接口
 */
public interface WeChatService {

    /**
     * 获取accessToken
     * @return accessToken
     */
    BaseResponse getAccessToken();

    /**
     * 生成带参数临时的二维码
     * @return 临时的二维码地址
     */
    BaseResponse loginQrCode(String accessToken);
    /**
     * 生成带参数永久的二维码
     * @return 永久的二维码地址
     */
    BaseResponse loginLimitQrCode(String accessToken);
}
