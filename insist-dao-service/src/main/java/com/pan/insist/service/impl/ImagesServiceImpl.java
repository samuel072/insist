package com.pan.insist.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pan.insist.dao.ImagesMapper;
import com.pan.insist.model.ImagesModel;
import com.pan.insist.service.ImagesService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * fastFDS图片 服务实现类
 * </p>
 *
 * @author 黄阿能
 * @since 2020-01-16
 */
@Service
public class ImagesServiceImpl extends ServiceImpl<ImagesMapper, ImagesModel> implements ImagesService {

}
