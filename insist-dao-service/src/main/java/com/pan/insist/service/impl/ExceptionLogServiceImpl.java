package com.pan.insist.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pan.insist.dao.ExceptionLogMapper;
import com.pan.insist.model.ExceptionLogModel;
import com.pan.insist.service.ExceptionLogService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 异常信息日志表 服务实现类
 * </p>
 *
 * @author 黄阿能
 * @since 2020-01-20
 */
@Service
public class ExceptionLogServiceImpl extends ServiceImpl<ExceptionLogMapper, ExceptionLogModel> implements ExceptionLogService {

}
