package com.pan.insist.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pan.insist.model.UserModel;

import java.util.Set;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author kaiji
 * @since 2019-08-17
 */
public interface UserService extends IService<UserModel> {

    /**
     * 查询手机号是否存在
     *
     * @param id
     *      用户主键
     * @param phone
     *      手机号码
     * @return
     *      用户对象
     */
    UserModel getUserByExist(String id, String phone);

    /**
     * 根据手机号查询角色代码code列表
     *
     * @param phone
     *      手机号
     * @return
     *      角色code列表
     */
    Set<String> getRoleByPhone(String phone);

    /**
     * 修改用户信息
     *
     * @param userModel
     *      用户信息
     * @return true ：成功 false：失败
     */
    boolean updateByUserInfo(UserModel userModel);

    /**
     * 保存用户与角色的关系
     *
     * @param userModel
     *      用户信息
     * @return true：成功 false：失败
     */
    void saveAndRole(UserModel userModel);
}
