package com.pan.insist.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pan.insist.model.TaskCronModel;

/**
 * <p>
 * 任务管理表 服务类
 * </p>
 *
 * @author 黄阿能
 * @since 2020-02-07
 */
public interface TaskCronService extends IService<TaskCronModel> {

}
