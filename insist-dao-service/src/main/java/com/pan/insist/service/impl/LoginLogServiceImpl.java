package com.pan.insist.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pan.insist.dao.LoginLogMapper;
import com.pan.insist.model.LoginLogModel;
import com.pan.insist.service.LoginLogService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 登录日志表 服务实现类
 * </p>
 *
 * @author 黄阿能
 * @since 2020-01-15
 */
@Service
public class LoginLogServiceImpl extends ServiceImpl<LoginLogMapper, LoginLogModel> implements LoginLogService {

}
