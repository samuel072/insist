package com.pan.insist.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pan.insist.model.MenuModel;

import java.util.List;

/**
 * <p>
 * 菜单信息表 服务类
 * </p>
 *
 * @author kaiji
 * @since 2019-08-17
 */
public interface MenuService extends IService<MenuModel> {

    /**
     *  菜单是否存在
     *
     * @param menuModel
     *      菜单条件查询对象
     * @return  菜单对象
     */
    MenuModel getExistByFilter(MenuModel menuModel);

    /**
     * 查询最上级的菜单
     *
     * @return  菜单对象列表
     */
    List<MenuModel> getTopMenu();

    /**
     *  获取子节点列比爱哦
     *
     * @param id
     *      ID主键
     * @return 菜单列表
     */
    List<MenuModel> getChildren(String id);

    /**
     * 根据用户查询用户相关菜单
     *
     * @param userId
     *      用户主键
     * @return 自己的菜单列表
     */
    List<MenuModel> getByUser(String userId);

    /**
     * 根据用户主键查询这个用户下所有的菜单栏<br/>
     *
     * @param userId
     *     用户主键
     * @return  导航栏菜单列表
     */
    List<MenuModel> getPageMainMenuByUser(String userId);
}
