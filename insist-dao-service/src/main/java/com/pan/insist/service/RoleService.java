package com.pan.insist.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pan.insist.model.RoleModel;

import java.util.List;

/**
 * <p>
 * 角色信息表 服务类
 * </p>
 *
 * @author kaiji
 * @since 2019-08-17
 */
public interface RoleService extends IService<RoleModel> {

    /**
     * 根据角色名称或代码查询是否存在数据
     *
     * @param roleModel
     *      角色对象
     * @return  角色对象
     */
    RoleModel getExistByFilter(RoleModel roleModel);

    /**
     * 根据角色code查询菜单地址列表
     *
     * @param codeList
     *      角色代码集合
     * @return  菜单地址列表
     */
    List<String> getUrlByRole(List<String> codeList);

    /**
     * 保存角色与权限的关系
     *
     * @param roleId
     *      角色主键
     * @param purviewIds
     *      权限主键集合
     * @throws Exception 异常
     */
    void saveRoleAndPurview(String roleId, String purviewIds) throws Exception;

    /**
     * 根据用户查询这个用户所有拥有的角色主键集合
     *
     * @param userId
     *      用户主键
     * @return  角色主键
     */
    List<String> getRoleIdsByUserId(String userId);
}
