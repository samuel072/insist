package com.pan.insist.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pan.insist.dao.OperateLogMapper;
import com.pan.insist.model.OperateLogModel;
import com.pan.insist.service.OperateLogService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 操作日志表 服务实现类
 * </p>
 *
 * @author 黄阿能
 * @since 2020-01-20
 */
@Service
public class OperateLogServiceImpl extends ServiceImpl<OperateLogMapper, OperateLogModel> implements OperateLogService {

}
