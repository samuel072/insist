package com.pan.insist.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pan.insist.model.LoginLogModel;

/**
 * <p>
 * 登录日志表 服务类
 * </p>
 *
 * @author 黄阿能
 * @since 2020-01-15
 */
public interface LoginLogService extends IService<LoginLogModel> {

}
