package com.pan.insist.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pan.insist.dao.TestMapper;
import com.pan.insist.model.TestModel;
import com.pan.insist.service.TestService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 测试 服务实现类
 * </p>
 *
 * @author 黄阿能
 * @since 2020-01-09
 */
@Service
public class TestServiceImpl extends ServiceImpl<TestMapper, TestModel> implements TestService {

}
