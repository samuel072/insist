package com.pan.insist.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pan.insist.dao.TaskCronMapper;
import com.pan.insist.model.TaskCronModel;
import com.pan.insist.service.TaskCronService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 任务管理表 服务实现类
 * </p>
 *
 * @author 黄阿能
 * @since 2020-02-07
 */
@Service
public class TaskCronServiceImpl extends ServiceImpl<TaskCronMapper, TaskCronModel> implements TaskCronService {

}
