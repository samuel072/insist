package com.pan.insist.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pan.insist.model.ImagesModel;

/**
 * <p>
 * fastFDS图片 服务类
 * </p>
 *
 * @author 黄阿能
 * @since 2020-01-16
 */
public interface ImagesService extends IService<ImagesModel> {

}
