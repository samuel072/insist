package com.pan.insist.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pan.insist.model.ExceptionLogModel;

/**
 * <p>
 * 异常信息日志表 服务类
 * </p>
 *
 * @author 黄阿能
 * @since 2020-01-20
 */
public interface ExceptionLogService extends IService<ExceptionLogModel> {

}
