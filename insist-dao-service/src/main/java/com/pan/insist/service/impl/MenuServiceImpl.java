package com.pan.insist.service.impl;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pan.insist.constant.CommonConstant;
import com.pan.insist.dao.MenuMapper;
import com.pan.insist.model.MenuModel;
import com.pan.insist.service.MenuService;
import com.pan.insist.service.RoleService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * <p>
 * 菜单信息表 服务实现类
 * </p>
 *
 * @author kaiji
 * @since 2019-08-17
 */
@Service
public class MenuServiceImpl extends ServiceImpl<MenuMapper, MenuModel> implements MenuService {

    @Resource
    private MenuMapper menuMapper;
    @Resource
    private RoleService roleService;

    @Override
    public MenuModel getExistByFilter(MenuModel menuModel) {
        if (StringUtils.isBlank(menuModel.getId())) {
            menuModel.setId(null);
        }
        return menuMapper.getExistByFilter(menuModel);
    }

    @Override
    public List<MenuModel> getTopMenu() {
        return menuMapper.getTopMenu();
    }

    @Override
    public List<MenuModel> getChildren(String id) {
        return menuMapper.getChildren(id);
    }

    @Override
    public List<MenuModel> getByUser(String userId) {
        List<String> roleIds = roleService.getRoleIdsByUserId(userId);
        Map<String, Object> filterMap = new HashMap<>();
        filterMap.put("roleIds", roleIds);
        return Optional.ofNullable(menuMapper.getByRoles(filterMap)).orElse(new ArrayList<>());
    }

    @Override
    public List<MenuModel> getPageMainMenuByUser(String userId) {
        List<String> roleIds = roleService.getRoleIdsByUserId(userId);
        Map<String, Object> filterMap = new HashMap<>();
        filterMap.put("roleIds", roleIds);
        filterMap.put("menu", CommonConstant.MAGIC_VALUE_ONE);
        return Optional.ofNullable(menuMapper.getByRoles(filterMap)).orElse(new ArrayList<>());
    }
}
