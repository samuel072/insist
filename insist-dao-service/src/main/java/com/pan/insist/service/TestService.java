package com.pan.insist.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pan.insist.model.TestModel;

/**
 * <p>
 * 测试 服务类
 * </p>
 *
 * @author 黄阿能
 * @since 2020-01-09
 */
public interface TestService extends IService<TestModel> {

}
