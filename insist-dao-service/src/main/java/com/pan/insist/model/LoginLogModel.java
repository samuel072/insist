package com.pan.insist.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.pan.insist.base.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 登录日志表
 * </p>
 *
 * @author 黄阿能
 * @since 2020-01-15
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("t_login_log")
public class LoginLogModel extends BaseModel {

    private static final long serialVersionUID = -6947276184486696909L;
    /**
     * 登录用户
     */
    @TableField("login_name")
    private String loginName;

    /**
     * 日志名称
     */
    @TableField("log_name")
    private String logName;

    /**
     * ip地址

     */
    @TableField("ip")
    private String ip;


}
