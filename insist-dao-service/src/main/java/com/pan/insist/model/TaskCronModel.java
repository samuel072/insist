package com.pan.insist.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.pan.insist.base.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 任务管理表
 * </p>
 *
 * @author 黄阿能
 * @since 2020-02-07
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("t_task_cron")
public class TaskCronModel extends BaseModel {

    private static final long serialVersionUID = 1L;

    /**
     * 任务名称
     */
    @TableField("name")
    private String name;

    /**
     * 任务编号
     */
    @TableField("code")
    private String code;

    /**
     * 任务执行规则
     */
    @TableField("cron")
    private String cron;


}
