package com.pan.insist.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.pan.insist.base.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 异常信息日志表
 * </p>
 *
 * @author 黄阿能
 * @since 2020-01-20
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("t_exception_log")
public class ExceptionLogModel extends BaseModel {

    private static final long serialVersionUID = 1L;

    /**
     * 请求参数
     */
    @TableField("request_params")
    private String requestParams;

    /**
     * 返回参数
     */
    @TableField("response_params")
    private String responseParams;

    /**
     * 异常名称
     */
    @TableField("exception_name")
    private String exceptionName;

    /**
     * 异常信息
     */
    @TableField("message")
    private String message;

    /**
     * 操作员ID
     */
    @TableField("user_id")
    private String userId;

    /**
     * 操作员名称
     */
    @TableField("user_name")
    private String userName;

    /**
     * 访问方法
     */
    @TableField("method")
    private String method;

    /**
     * 请求地址
     */
    @TableField("url")
    private String url;

    /**
     * 访问IP
     */
    @TableField("ip")
    private String ip;


}
