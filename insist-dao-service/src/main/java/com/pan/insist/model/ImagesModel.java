package com.pan.insist.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.pan.insist.base.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * fastFDS图片
 * </p>
 *
 * @author 黄阿能
 * @since 2020-01-16
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("t_images")
public class ImagesModel extends BaseModel {

    private static final long serialVersionUID = 1L;

    /**
     * 图片地址
     */
    @TableField("url")
    private String url;

    /**
     * 1: 未删除 2：删除
     */
    @TableField("deletion")
    private Boolean deletion;


}
