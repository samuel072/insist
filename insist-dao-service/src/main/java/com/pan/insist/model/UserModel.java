package com.pan.insist.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.pan.insist.base.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 用户表
 * </p>
 *
 * @author kaiji
 * @since 2019-08-17
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("t_user")
public class UserModel extends BaseModel {

    private static final long serialVersionUID = 1L;

    /**
     * 展示姓名
     */
    @TableField("username")
    private String username;

    /**
     * 手机号
     */
    @TableField("phone")
    private String phone;

    /**
     * 短信验证码
     */
    @TableField("auth_code")
    private String authCode;

    /**
     * 密码盐
     */
    @TableField("salt")
    private String salt;

    /**
     * 密码
     */
    @TableField("password")
    private String password;

    /**
     * 登录IP
     */
    @TableField("login_ip")
    private String loginIp;

    /**
     * 登录时间
     */
    @TableField("login_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime loginDate;

    /**
     * 0: 启用 1：删除
     */
    @TableField("status")
    private Integer status;

    /**
     * 角色id集合
     */
    @TableField(exist = false)
    private List<String> roleIds;

    /**
     * 用户头像
     */
    @TableField
    private String avatar;


}
