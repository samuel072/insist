package com.pan.insist.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.pan.insist.base.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * <p>
 * 菜单信息表
 * </p>
 *
 * @author kaiji
 * @since 2019-08-17
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("t_menu")
public class MenuModel extends BaseModel {
    private static final long serialVersionUID = -3923390293343416887L;

    /**
     * 菜单名称
     */
    @TableField("name")
    private String name;

    /**
     * 菜单地址
     */
    @TableField("url")
    private String url;

    /**
     * 父菜单ID 0：顶级 其他：子菜单
     */
    @TableField("pid")
    private String pid;

    /**
     * 是否是菜单 1：菜单 2：按钮
     */
    @TableField("menu")
    private int menu;

    /**
     * 菜单图标
     */
    @TableField("icon")
    private String icon;

    /**
     * 排序， 越小越前
     */
    @TableField("order_num")
    private int orderNum;

    @TableField(exist = false)
    private String pidName;
}
