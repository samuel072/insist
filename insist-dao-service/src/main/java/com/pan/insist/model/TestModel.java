package com.pan.insist.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.pan.insist.base.BaseModel;
import com.pan.insist.myabtis.CryptTypeHandler;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

/**
 * <p>
 * 测试
 * </p>
 *
 * @author 黄阿能
 * @since 2020-01-09
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName(value = "t_test", autoResultMap = true)
public class TestModel extends BaseModel {
    private static final long serialVersionUID = 4710927091100830097L;

    /**
     * 测试人
     */
    @TableField("test_name")
    private String testName;

    /**
     * 测试事件
     */
    @TableField(value = "test_event", typeHandler = CryptTypeHandler.class)
    private String testEvent;

    /**
     * 修改时间
     */
    @TableField("modity_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime modityDate;


}
