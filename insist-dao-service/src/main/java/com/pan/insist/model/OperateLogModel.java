package com.pan.insist.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.pan.insist.base.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 操作日志表
 * </p>
 *
 * @author kaiji
 * @since 2020-01-20
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("t_operate_log")
public class OperateLogModel extends BaseModel {

    private static final long serialVersionUID = -6896259941254657463L;
    /**
     * 操作模块
     */
    @TableField("module")
    private String module;

    /**
     * 操作类型
     */
    @TableField("type")
    private String type;

    /**
     * 操作描述
     */
    @TableField("description")
    private String description;

    /**
     * 请求参数
     */
    @TableField("request_params")
    private String requestParams;

    /**
     * 返回参数
     */
    @TableField("response_params")
    private String responseParams;

    /**
     * 用户主键
     */
    @TableField("user_id")
    private String userId;

    /**
     * 用户名称
     */
    @TableField("user_name")
    private String userName;

    /**
     * 操作方法
     */
    @TableField("method")
    private String method;

    /**
     * 访问地址
     */
    @TableField("url")
    private String url;

    /**
     * 访问IP
     */
    @TableField("ip")
    private String ip;
}
