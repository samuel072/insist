package com.pan.insist.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pan.insist.cache.MybatisRedisCache;
import com.pan.insist.model.MenuModel;
import org.apache.ibatis.annotations.CacheNamespace;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 菜单信息表 Mapper 接口
 * </p>
 *
 * @author kaiji
 * @since 2019-08-17
 */
@Mapper
@CacheNamespace(implementation= MybatisRedisCache.class, eviction= MybatisRedisCache.class)
public interface MenuMapper extends BaseMapper<MenuModel> {

    /**
     *  菜单是否存在
     *
     * @param menuModel
     *      菜单条件查询对象
     * @return  菜单对象
     */
    @Select("<script>select a.* from t_menu as a where (a.name = #{name} or a.url = #{url}) " +
            "<if test='id != null'>and a.id != #{id}</if> </script>")
    MenuModel getExistByFilter(MenuModel menuModel);

    /**
     * 查询最上级的菜单
     *
     * @return  菜单对象列表
     */
    @Select("<script>select a.* from t_menu as a where a.pid is null or a.pid = 0</script>")
    List<MenuModel> getTopMenu();

    /**
     *  获取子节点菜单列表
     *
     * @param id
     *      ID主键
     * @return 菜单列表
     */
    @Select("<script>select a.* from t_menu as a where a.pid = #{id}</script>")
    List<MenuModel> getChildren(String id);

    /**
     * 根据角色查询菜单列表
     *
     * @param filterMap
     *      角色id主键 、 是否是菜单 1：菜单 2：按钮
     * @return  菜单列表
     */
    @Select("<script>select b.* from t_role as a, t_menu as b, t_menu_role as c where a.id = c.role_id " +
            "and b.id = c.menu_id and a.id in " +
            "<foreach collection='roleIds' open='(' item='roleId' separator=',' close=')'> #{roleId}</foreach> " +
            "<if test='menu != null'>and b.menu = #{menu}</if>" +
            "</script>")
    List<MenuModel> getByRoles(Map<String, Object> filterMap);
}
