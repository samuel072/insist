package com.pan.insist.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pan.insist.cache.MybatisRedisCache;
import com.pan.insist.model.LoginLogModel;
import org.apache.ibatis.annotations.CacheNamespace;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 登录日志表 Mapper 接口
 * </p>
 *
 * @author 黄阿能
 * @since 2020-01-15
 */
@Mapper
@CacheNamespace(implementation= MybatisRedisCache.class, eviction= MybatisRedisCache.class)
public interface LoginLogMapper extends BaseMapper<LoginLogModel> {

}
