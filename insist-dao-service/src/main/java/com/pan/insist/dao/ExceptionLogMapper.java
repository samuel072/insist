package com.pan.insist.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pan.insist.cache.MybatisRedisCache;
import com.pan.insist.model.ExceptionLogModel;
import org.apache.ibatis.annotations.CacheNamespace;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 异常信息日志表 Mapper 接口
 * </p>
 *
 * @author 黄阿能
 * @since 2020-01-20
 */
@Mapper
@CacheNamespace(implementation= MybatisRedisCache.class, eviction= MybatisRedisCache.class)
public interface ExceptionLogMapper extends BaseMapper<ExceptionLogModel> {

}
