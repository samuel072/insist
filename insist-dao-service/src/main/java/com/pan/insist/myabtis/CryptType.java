package com.pan.insist.myabtis;

import org.apache.ibatis.type.Alias;

/**
 * <p>mybatis的别称</p>
 *
 * @author aneng huang
 * @since 2020/4/27 9:52 上午
 **/
@Alias("cryptType")
public class CryptType {
}
