package com.pan.insist.cache.redis;

import com.alibaba.fastjson.JSON;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.util.Assert;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * 使用StringRedisSerializer做key的序列化时，StringRedisSerializer的泛型指定的是String，
 * 传其他对象就会报类型转换错误，在使用@Cacheable注解是key属性就只能传String进来。把这个序列化方式重写了，将泛型改成Object
 */
public class StringRedisSerializer implements RedisSerializer<Object> {
    private final Charset charset;

    public StringRedisSerializer() {
        this(StandardCharsets.UTF_8);
    }

    private StringRedisSerializer(Charset charset) {
        Assert.notNull(charset, "Charset must not be null!");
        this.charset = charset;
    }

    @Override
    public String deserialize(byte[] bytes) {
        return (bytes == null ? null : new String(bytes, charset));
    }

    @Override
    public byte[] serialize(Object object) {
        String str = JSON.toJSONString(object);
        if (StringUtils.isBlank(str)) {
            return null;
        }
        String target = "\"";
        String replacement = "";
        str = str.replace(target, replacement);
        return str.getBytes(charset);
    }

}