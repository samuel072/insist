package com.pan.insist.cache.redis;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.SerializationException;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * @author liuyazhuang
 * @date 2018/8/15 23:23
 * @description  自定义GenericFastJson2JsonRedisSerializer序列化器，其中用到了FastJsonWrapper包装类
 * @version 1.0.0
 */
public class GenericFastJson2JsonRedisSerializer<T> implements RedisSerializer<T> {

    private static final Charset DEFAULT_CHARSET = StandardCharsets.UTF_8;
    public GenericFastJson2JsonRedisSerializer() {
        super();
    }

    @Override
    public byte[] serialize(T t) throws SerializationException {
        if (t == null) {
            return new byte[0];
        }
        FastJsonWrapper<T> wrapperSet = new FastJsonWrapper<>(t);
        return JSON.toJSONString(wrapperSet, SerializerFeature.WriteClassName).getBytes(DEFAULT_CHARSET);
    }

    @Override
    @SuppressWarnings("unchecked")
    public T deserialize(byte[] bytes) throws SerializationException {
        if (bytes == null || bytes.length <= 0) {
            return null;
        }
        String deserializeStr = new String(bytes, DEFAULT_CHARSET);
		FastJsonWrapper<T> wrapperGet = JSON.parseObject(deserializeStr, FastJsonWrapper.class);
        return wrapperGet.getValue();
    }
}