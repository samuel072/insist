package com.pan.insist.cache;

import com.pan.insist.constant.CommonConstant;
import com.pan.insist.util.SpringUtil;
import com.pan.insist.util.StrUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.cache.Cache;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.connection.RedisServerCommands;
import org.springframework.data.redis.core.Cursor;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ScanOptions;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Consumer;

/**
 * 实现数据缓存
 *
 * @author kaiji
 * @since 2019/9/10 14:02
 */
@Slf4j
public class MybatisRedisCache implements Cache {


    /** 读写锁 **/
    private final ReadWriteLock readWriteLock = new ReentrantReadWriteLock(true);

    /** 这里使用了redis缓存，使用springboot自动注入 **/
    private RedisTemplate<String, Object> redisTemplate;

    private String id;

    public MybatisRedisCache(final String id) {
        if (id == null) {
            throw new IllegalArgumentException("Cache instances require an ID");
        }
        this.id = id;
    }

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public void putObject(Object key, Object value) {
        getRedis();
        if (value != null) {
            String keyStr = key.toString();
            keyStr = StrUtil.replaceLineBlanks(keyStr);
            redisTemplate.opsForValue().set(keyStr, value);
        }
    }

    @Override
    public Object getObject(Object key) {
        getRedis();
        try {
            if (key != null) {
                key = StrUtil.replaceLineBlanks(key.toString());
                return redisTemplate.opsForValue().get(key.toString());
            }
        } catch (Exception e) {
            log.error("缓存出错 {}", e.getMessage());
        }
        return null;
    }

    @Override
    public Object removeObject(Object key) {
        getRedis();
        if (key != null) {
            key = StrUtil.replaceLineBlanks(key.toString());
            return redisTemplate.delete(key.toString());
        }
        return null;
    }

    @Override
    public void clear() {
        getRedis();

        String sb = CommonConstant.ASTERISK +
                CommonConstant.COLON_FLAG +
                this.id +
                CommonConstant.ASTERISK;
        List<String> keys = this.keys(sb);
        if (!CollectionUtils.isEmpty(keys)) {
            redisTemplate.delete(keys);
        }
    }

    @Override
    public int getSize() {
        getRedis();
        Long size = redisTemplate.execute(RedisServerCommands::dbSize);
        assert size != null;
        return size.intValue();
    }

    @Override
    public ReadWriteLock getReadWriteLock() {
        return this.readWriteLock;
    }

    /**
     * 获取redis连接对象
     */
    @SuppressWarnings("unchecked")
    private void getRedis() {
        if (redisTemplate == null) {
            //由于启动期间注入失败，只能运行期间注入，这段代码可以删除
            log.info("redisTemplate为空，使用SpringUtil获取对象！");
            redisTemplate = (RedisTemplate<String, Object>) SpringUtil.getBean("redisTemplate");
        }
    }

    /**
     * scan 实现
     * @param pattern	表达式
     * @param consumer	对迭代到的key进行操作
     */
    private void scan(String pattern, Consumer<byte[]> consumer) {
        getRedis();
        this.redisTemplate.execute((RedisConnection connection) -> {
            try (Cursor<byte[]> cursor = connection.scan(ScanOptions.scanOptions().count(Long.MAX_VALUE).match(pattern).build())) {
                cursor.forEachRemaining(consumer);
                return null;
            } catch (IOException e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        });
    }

    /**
     * 获取符合条件的key
     * @param pattern	表达式
     * @return  keys
     */
    private List<String> keys(String pattern) {
        List<String> keys = new ArrayList<>();
        this.scan(pattern, item -> {
            //符合条件的key
            String key = new String(item, StandardCharsets.UTF_8);
            keys.add(key);
        });
        return keys;
    }
}