package com.pan.insist.cache.redis;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author liuyazhuang
 * @date 2018/8/15 23:15
 * @description FastJsonWrapper包装类
 * @version 1.0.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FastJsonWrapper<T> {
    private T value;
}