# insist

#### Description
1. 改造日志
2. layui登录页面
3. 代码生成器


#### 做这个项目出现的问题， 解决思路

问题1：想做最简配置出现的问题
因为想做最最简jar导入，所以在导入spring-boot-web jar包之后，没有引入模板引擎jar 导致每次return视图的时候，spring都将进入的地址当做了controller的访问路径

问题2：集成mybatis-plus的分页功能
看到配置分页功能的时候， 写错了注解 应该是 @configuration 但是随手选择了@Configurable 导致分页功能一直都不能实现。我差点一致都认为mybatis-plus的分页功能有bug。

问题3：选择springboot版本2.1.7
本来是想选择最新的版本2.1.7的， 但是不知道为什么导入到maven之后， 会一直有一个maven configuration problem 错误， 不知道怎么解决。降低版本2.0.3 问题解决。

问题4：maven打包的时候 出现 Execution default of goal org.springframework.boot:spring-boot-maven-plugin:2.0.3.RELEASE:repackage failed: Unable to find main class
是因为使用
	<plugin>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-maven-plugin</artifactId>
    </plugin>
    插件编译项目的时候，必须要有Application.java @springbootApplication这个main方法类

#### 每日更新
2019-08-15
1. 导入layui
2. 写一个登陆页面
3. 研究代码生成器（带页面的那种）
