<#noparse><#include "../common/common_ctx.ftl" /></#noparse>
<!DOCTYPE html>
<html	xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <#noparse><link rel="stylesheet" type="text/css" href="${ctx}/static/plugins/layuiadmin/layui/css/layui.css"/></#noparse>
    <#noparse><link rel="stylesheet" type="text/css" href="${ctx}/static/plugins/layuiadmin/style/admin.css"/></#noparse>
    <title>${cfg.context.bizChName}列表管理</title>
</head>
<body>
<div class="layui-fluid">
    <#-- 条件查询框 -->
    <div class="layui-card">
        <div class="layui-form layui-card-header layuiadmin-card-header-auto">
            <div class="layui-form-item">
                <div class="layui-inline">
                    <label class="layui-form-label">用户名</label>
                    <div class="layui-input-block">
                        <input class="layui-input" type="text" name="username" placeholder="请输入用户名" autocomplete="off">
                    </div>
                </div>
                <#noparse><@shiro.hasPermission name="/</#noparse>${cfg.context.bizEnName}<#noparse>/list"></#noparse>
                <#-- 搜索🔍框 -->
                <div class="layui-inline">
                    <button class="layui-btn layuiadmin-btn-admin" lay-submit lay-filter="LAY-search">
                        <i class="layui-icon layui-icon-search layuiadmin-button-btn"></i>
                    </button>
                </div>
                <#noparse></@shiro.hasPermission></#noparse>
            </div>
        </div>

        <#-- 表单 -->
        <div class="layui-card-body">
            <table class="layui-table" lay-filter="${cfg.context.bizEnName}Table" id="${cfg.context.bizEnName}Table"></table>
        </div>
    </div>
</div>

<#-- 工具栏 -->
<script type="text/html" id="tableHeadBar">
    <#noparse><@shiro.hasPermission name="/</#noparse>${cfg.context.bizEnName}<#noparse>/save"></#noparse>
    <button type="button" class="layui-btn layuiadmin-btn-admin layui-btn-sm layui-btn-normal" lay-event="add">
        <i class="layui-icon">&#xe654;</i> 添加
    </button>
    <#noparse></@shiro.hasPermission></#noparse>
    <#noparse><@shiro.hasPermission name="/</#noparse>${cfg.context.bizEnName}<#noparse>/delete"></#noparse>
    <button type="button" class="layui-btn layuiadmin-btn-admin layui-btn-sm layui-btn-danger" lay-event="del">
        <i class="layui-icon">&#xe640;</i> 删除
    </button>
    <#noparse></@shiro.hasPermission></#noparse>
</script>
<script type="text/html" id="tableRightBar">
    <#noparse><@shiro.hasPermission name="/</#noparse>${cfg.context.bizEnName}<#noparse>/view"></#noparse>
    <a class="layui-btn layui-btn-xs" lay-event="view">查看</a>
    <#noparse></@shiro.hasPermission></#noparse>
    <#noparse><@shiro.hasPermission name="/</#noparse>${cfg.context.bizEnName}<#noparse>/update"></#noparse>
    <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
    <#noparse></@shiro.hasPermission></#noparse>
</script>

<#noparse><#include "../common/__ctx.ftl" /></#noparse>
<script type="text/javascript" src="<#noparse>${ctx}</#noparse>/static/admin/modular/${cfg.context.bizEnName}.js"></script>

</body>
</html>