<#noparse><#include "../common/common_ctx.ftl" /></#noparse>
<!DOCTYPE html>
<html	xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <#noparse><link rel="stylesheet" type="text/css" href="${ctx}/static/plugins/layuiadmin/layui/css/layui.css"/></#noparse>
    <#noparse><link rel="stylesheet" type="text/css" href="${ctx}/static/admin/css/view.css"/></#noparse>
    <title>查看${cfg.context.bizChName}详细信息</title>
</head>
<body>

<div class="layui-form" style="padding: 20px 30px 0 0;">
    <#list table.fields as item>
        <div class="layui-form-item">
            <label class="layui-form-label">${item.comment}</label>
            <div class="layui-input-block">
                <#if item.columnType == 'LOCAL_DATE_TIME'>
                    <#noparse>${(</#noparse>${cfg.context.bizEnName}<#noparse>Model.</#noparse>${item.propertyName}<#noparse>)?replace("T", " ")!}</#noparse>
                <#else>
                    <#noparse>${(</#noparse>${cfg.context.bizEnName}<#noparse>Model.</#noparse>${item.propertyName}<#noparse>)!}</#noparse>
                </#if>
            </div>
        </div>
    </#list>
</div>
</body>
</html>