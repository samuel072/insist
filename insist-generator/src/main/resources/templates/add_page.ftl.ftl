<#noparse><#include "../common/common_ctx.ftl" /></#noparse>
<!DOCTYPE html>
<html	xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="UTF-8">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <#noparse><link rel="stylesheet" type="text/css" href="${ctx}/static/plugins/layuiadmin/layui/css/layui.css"/></#noparse>
    <#--    <link rel="stylesheet" type="text/css" href="${ctx}/static/plugins/layuiadmin/style/admin.css"/>-->
    <#--    <link rel="stylesheet" type="text/css" href="${ctx}/static/admin/css/base.css"/>-->
    <title><#noparse><#if</#noparse> ${cfg.context.bizEnName}<#noparse>Model?? &&</#noparse>
        ${cfg.context.bizEnName}<#noparse>Model.id??>编辑<#else>添加</#if></#noparse>${cfg.context.bizChName}详细信息</title>
</head>
<body>

<div class="layui-form" lay-filter="form-submit" style="padding: 20px 30px 0 0;">
    <#noparse><input type="hidden" name="id" value="${(</#noparse>${cfg.context.bizEnName}<#noparse>Model.id)!}" /></#noparse>
    <#list table.fields as item>
        <div class="layui-form-item">
            <label class="layui-form-label">${item.comment}</label>
            <div class="layui-input-inline">
                <#if item.columnType == 'LOCAL_DATE_TIME'>
                    <#assign dateTimeFlag="true"/>
                    <input type="text" class="layui-input date-class" name="${item.propertyName}" value="<#noparse><#if</#noparse> ${cfg.context.bizEnName}<#noparse>Model?? && </#noparse>${cfg.context.bizEnName}<#noparse>Model.</#noparse>${item.propertyName}<#noparse>??>${(</#noparse>${cfg.context.bizEnName}<#noparse>Model.</#noparse>${item.propertyName}<#noparse>)?replace("T", " ")!}</#if></#noparse>" autocomplete="off"
                           lay-verify="required" lay-reqtext="请填写正确的${item.comment}" />
                <#else>
                    <#assign dateTimeFlag="false"/>
                    <input type="text" class="layui-input" name="${item.propertyName}" value="<#noparse>${(</#noparse>${cfg.context.bizEnName}<#noparse>Model.</#noparse>${item.propertyName}<#noparse>)!}</#noparse>" autocomplete="off"
                           lay-verify="required" lay-reqtext="请填写正确的${item.comment}" />
                </#if>

            </div>
        </div>
    </#list>

    <div class="layui-form-item layui-hide">
        <input type="button" lay-submit lay-filter="LAY-back-submit" id="LAY-back-submit" value="确认">
    </div>
</div>
<#noparse >
<script type="text/javascript" src="${ctx}/static/plugins/layuiadmin/layui/layui.js"></script>
</#noparse>
<script type="text/javascript">
    <#if dateTimeFlag == "true">
    layui.use(['form', 'laydate'], function() {
        var form = layui.form;
        var laydate = layui.laydate;
        lay('.date-class').each(function(){
            laydate.render({
                elem: this,
                type: 'datetime',
                trigger: 'click'
            });
        });

    });
    <#else>
    layui.use(['form'], function() {
        var form = layui.form;
    });
    </#if>


</script>

</body>
</html>