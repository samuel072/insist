layui.config({
    base: Insist.ctx + '/static/plugins/layuiadmin/' //静态资源所在路径
}).extend({
    index: 'lib/index' //主入口模块
}).use(['index', 'form', 'table', 'setter'], function () {
    var table = layui.table;
    var config = layui.setter;
    var form = layui.form;

    var manage${cfg.context.bizEnBigName} = {
        tableId: "${cfg.context.bizEnName}Table",
        condition: {
            username: ""
        }
    };

    manage${cfg.context.bizEnBigName}.initColumn = function () {
        return [[
            {type: 'checkbox'},
            <#list table.fields as item>
            {title: '${item.comment}', field: '${item.propertyName}', visible: true, align: 'center', valign: 'middle'},
            </#list>
            {fixed: 'right', title: '操作', toolbar: '#tableRightBar', width: 120}
        ]];
    };

    // 查询条件
    manage${cfg.context.bizEnBigName}.search = function (data) {
        table.reload(manage${cfg.context.bizEnBigName}.tableId, {where: data});
    };

    // 搜索
    form.on("submit(LAY-search)", function (data) {
        manage${cfg.context.bizEnBigName}.search(data.field);
    });

    manage${cfg.context.bizEnBigName}.view = function (data) {
        var pageIndex = layer.open({
            type: 2
            , title: '查看${cfg.context.bizChName}'
            , content: Insist.ctx + '/${cfg.context.bizEnName}/view?id=' + data.id
            , maxmin: true
            , btn: ['返回']
            , yes: function (index) {
                layer.close(index);
            }
        });
        layer.full(pageIndex);
    };

    manage${cfg.context.bizEnBigName}.edit = function (data) {
        Insist.commonFunction(table,'/${cfg.context.bizEnName}/update?id=' + data.id, manage${cfg.context.bizEnBigName}.tableId);
    };

    manage${cfg.context.bizEnBigName}.add = function() {
        Insist.commonFunction(table,'/${cfg.context.bizEnName}/save', manage${cfg.context.bizEnBigName}.tableId);
    };

    manage${cfg.context.bizEnBigName}.delete = function (data) {
        Insist.commonDel(table, "/${cfg.context.bizEnName}/delete", manage${cfg.context.bizEnBigName}.tableId, data);
    };

    // 监听头部事件
    table.on('toolbar(' + manage${cfg.context.bizEnBigName}.tableId + ')', function (obj) {
        var checkStatus = table.checkStatus(obj.config.id);
        var data = checkStatus.data;
        switch (obj.event) {
            case 'del':
                manage${cfg.context.bizEnBigName}.delete(data);
                break;
            case 'add':
                manage${cfg.context.bizEnBigName}.add();
                break;
        }
    });
    // 监听行工具事件
    table.on('tool(' + manage${cfg.context.bizEnBigName}.tableId + ')', function (obj) {
        var data = obj.data;
        switch (obj.event) {
            case 'view':
                manage${cfg.context.bizEnBigName}.view(data);
                break;
            case 'edit':
                manage${cfg.context.bizEnBigName}.edit(data);
                break;
        }
    });


    table.render({
        elem: '#' + manage${cfg.context.bizEnBigName}.tableId
        , url: Insist.ctx + '/${cfg.context.bizEnName}/listJson'
        , toolbar: '#tableHeadBar'
        , title: '${cfg.context.bizChName}数据表'
        , cellMinWidth: 80
        , cols: manage${cfg.context.bizEnBigName}.initColumn(),
        page: config.page,
        request: config.request
    });

});