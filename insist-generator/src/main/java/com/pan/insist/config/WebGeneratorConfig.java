package com.pan.insist.config;

import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.pan.insist.constant.CommonConstant;
import com.pan.insist.bean.GcBean;
import com.pan.insist.bean.PathConstant;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;

/**
 * @author kaiji
 */
public class WebGeneratorConfig extends AbstractConfig {

    private GcBean gcBean;

    public WebGeneratorConfig(GcBean gcBean) {
        this.gcBean = gcBean;
    }

    @Override
    protected void abstractConfig() {

        // 全局配置
        // 获取项目地址
        String projectPath = System.getProperty("user.dir");
        globalConfig.setOutputDir(projectPath + "/insist-admin/src/test/java");
        globalConfig.setAuthor(gcBean.getAuthorName());
        globalConfig.setOpen(false);
        globalConfig.setEntityName("%sModel");
        globalConfig.setMapperName("%sMapper");
        globalConfig.setServiceName("%sService");
        globalConfig.setFileOverride(false);

        // 数据源配置
        dataSourceConfig.setUrl(gcBean.getUrl());
        dataSourceConfig.setDriverName(gcBean.getDriverName());
        dataSourceConfig.setUsername(gcBean.getUsername());
        dataSourceConfig.setPassword(gcBean.getPassword());

        // 策略配置
        strategyConfig.setNaming(NamingStrategy.underline_to_camel);
        strategyConfig.setColumnNaming(NamingStrategy.underline_to_camel);
        // 生成的为lombok
        strategyConfig.setEntityLombokModel(false);
        strategyConfig.setRestControllerStyle(false);
        strategyConfig.setControllerMappingHyphenStyle(true);
        if (null != gcBean.getCommonFiled() && gcBean.getCommonFiled().length > 0) {
            strategyConfig.setSuperEntityColumns(gcBean.getCommonFiled());
        }

        // 公共父类
        strategyConfig.setSuperControllerClass(gcBean.getParentPackagePath()+".base.BaseController");
        strategyConfig.setSuperEntityClass(gcBean.getParentPackagePath()+".base.BaseModel");
        // 生成序列化id
        strategyConfig.setEntitySerialVersionUID(true);
        // 生成字段注释
        strategyConfig.setEntityTableFieldAnnotationEnable(true);

        strategyConfig.setInclude(gcBean.getTableName());
        strategyConfig.setTablePrefix(gcBean.getTablePrefix());

        // 包配置
        packageConfig.setParent(gcBean.getParentPackagePath());
        packageConfig.setMapper("dao");
        // 配置实体类名称
        packageConfig.setEntity("model");

        // 配置自定义的包的路径
        Map<String, String> pathInfo = new HashMap<>(CommonConstant.DEFAULT_INITIAL_CAPACITY);
        String outputDir = globalConfig.getOutputDir() + File.separator + gcBean.getParentPackagePath();
        outputDir = outputDir.replaceAll("\\.", Matcher.quoteReplacement(File.separator));
        // 模板信息
        if (!gcBean.getControllerSwitch()) {
            templateConfig.setController(null);
        } else {
            pathInfo.put(PathConstant.CONTROLLER_PATH, outputDir + File.separator + packageConfig.getController());
        }
        if (!gcBean.getServiceSwitch()) {
            templateConfig.setService(null);
            templateConfig.setServiceImpl(null);
        } else {
            pathInfo.put(PathConstant.SERVICE_PATH, outputDir + File.separator + packageConfig.getService());
            pathInfo.put(PathConstant.SERVICE_IMPL_PATH, outputDir + File.separator + packageConfig.getServiceImpl());
        }

        if (!gcBean.getDaoSwitch()) {
            templateConfig.setMapper(null);
        } else {
            pathInfo.put(PathConstant.MAPPER_PATH, outputDir + File.separator + packageConfig.getMapper());
        }

        if (!gcBean.getMapperSwitch()) {
            templateConfig.setXml(null);
        } else {
            pathInfo.put(PathConstant.XML_PATH, outputDir + File.separator + packageConfig.getXml());
        }

        if (!gcBean.getEntitySwitch()) {
            templateConfig.setEntity(null);
        } else {
            pathInfo.put(PathConstant.ENTITY_PATH, outputDir + File.separator + packageConfig.getEntity());
        }

        if (gcBean.getIndexPageSwitch()) {
            pathInfo.put(PathConstant.PAGE_PATH, outputDir + File.separator + PathConstant.PAGES);
        }

        if (gcBean.getJsSwitch()) {
            pathInfo.put(PathConstant.PAGES, outputDir + File.separator + PathConstant.PAGES);
        }

        if (gcBean.getAddPageSwitch()) {
            pathInfo.put(PathConstant.PAGE_INPUT, outputDir + File.separator + PathConstant.PAGES);
        }
        if (gcBean.getViewPageSwitch()) {
            pathInfo.put(PathConstant.PAGE_VIEW, outputDir + File.separator + PathConstant.PAGES);
        }
        packageConfig.setPathInfo(pathInfo);
        context = gcBean.getContext();
    }
}
