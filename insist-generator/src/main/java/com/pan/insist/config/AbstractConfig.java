package com.pan.insist.config;

import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.TemplateConfig;
import com.pan.insist.constant.CommonConstant;
import com.pan.insist.bean.Context;
import com.pan.insist.engine.InsistAutoGeneratorEngine;
import com.pan.insist.engine.InsistTemplateEngine;
import com.pan.insist.util.StrUtil;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * 配置
 *
 * @author kaiji
 */
public abstract class AbstractConfig {

    GlobalConfig globalConfig = new GlobalConfig();
    DataSourceConfig dataSourceConfig = new DataSourceConfig();
    StrategyConfig strategyConfig = new StrategyConfig();
    PackageConfig packageConfig = new PackageConfig();
    TemplateConfig templateConfig = new TemplateConfig();
    Context context = new Context();

    /** 自定义接口配置 */
    protected abstract void abstractConfig();

    private void init() {
        // 设置完配置
        abstractConfig();
    }

    /**
     * 开始生成魔板
     */
    public void doMpGeneration() {
        init();
        InsistAutoGeneratorEngine autoGenerator = new InsistAutoGeneratorEngine();
        autoGenerator.setGlobalConfig(globalConfig);
        autoGenerator.setDataSource(dataSourceConfig);
        autoGenerator.setStrategy(strategyConfig);
        autoGenerator.setPackageInfo(packageConfig);
        autoGenerator.setTemplate(templateConfig);
        autoGenerator.setTemplateEngine(new InsistTemplateEngine());
        // 自定义配置
        InjectionConfig injectionConfig = new InjectionConfig() {
            @Override
            public void initMap() {
            }
        };
        Map<String, Object> dataMap = new HashMap<>(CommonConstant.DEFAULT_INITIAL_CAPACITY);
        String bizEnName = context.getBizEnName();
        String s = StrUtil.lineToHump(bizEnName);
        context.setBizEnName(StringUtils.uncapitalize(s));
        context.setBizEnBigName(StringUtils.capitalize(s));
        dataMap.put("context", context);
        injectionConfig.setMap(dataMap);
        autoGenerator.setCfg(injectionConfig);
        autoGenerator.execute();
    }



}
