package com.pan.insist.generator;

import com.pan.insist.bean.GcBean;
import com.pan.insist.config.WebGeneratorConfig;

/**
 * @author kaiji
 */
public class GeneratorCode {

    /**
     * 开始生成代码
     *
     * @param gcBean
     *      代码对象
     */
    public void generatorCode(GcBean gcBean) {
        WebGeneratorConfig generatorConfig = new WebGeneratorConfig(gcBean);
        generatorConfig.doMpGeneration();
    }
}
