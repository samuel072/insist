package com.pan.insist.bean;

import lombok.Data;

/**
 * 全局配置
 *
 * @author kaiji
 * @since  2019-10-10
 */
@Data
public class Context {

    /** 业务名称 */
    private String bizChName = "用户管理";
    /** 业务英文名称 */
    private String bizEnName = "user";
    /** 业务英文名称(大写) */
    private String bizEnBigName = "User";
}
