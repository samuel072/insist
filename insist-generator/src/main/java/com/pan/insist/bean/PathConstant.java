package com.pan.insist.bean;

import com.baomidou.mybatisplus.generator.config.ConstVal;

/**
 * @author kaiji
 */
public class PathConstant implements ConstVal {
    /** 主页面 **/
    public static final String PAGES = "pages";
    /** 主页面JS **/
    public static final String PAGE_PATH = "page_path";
    /** input 页面 **/
    public static final String PAGE_INPUT = "input_path";
    /** view页面 **/
    public static final String PAGE_VIEW = "view_path";



}
