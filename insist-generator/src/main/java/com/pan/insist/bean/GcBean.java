package com.pan.insist.bean;

import lombok.Data;

/**
 *	代码生成对象 
 * @author kaiji
 */
@Data
public class GcBean {

	/** 数据库信息 */
	private String url;
	private String driverName;
	private String username;
	private String password;
	
	/** 其他信息 */
	private String authorName;
	private String parentPackagePath;
	private String tableName;
	private String tablePrefix;
	
	/** model公共类字段 多个逗号隔开 */
	private String[] commonFiled;

	private Context context;

	/** 是否生成控制器代码开关 */
	private Boolean controllerSwitch = true;
	/** 主页 */
	private Boolean indexPageSwitch = true;
	/** 添加页面 */
	private Boolean addPageSwitch = true;
	/** 查看页面 */
	private Boolean viewPageSwitch = true;
	/** js **/
	private Boolean jsSwitch = true;
	/** dao */
	private Boolean daoSwitch = true;
	/** xml mapper */
	private Boolean mapperSwitch = true;
	/** //service **/
	private Boolean serviceSwitch = true;
	/** 生成实体的开关 **/
	private Boolean entitySwitch = true;
	/** 生成sql的开关 */
	private Boolean sqlSwitch = true;

}
